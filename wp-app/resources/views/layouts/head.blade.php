<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name', 'Laravel') }}</title>
<meta name="description" content="{{ config('app.description', '') }}" />

<!-- Scripts -->
{{--{!! Assets::js() !!}--}}
{{--{!! var_dump(asset('js/app.js')); !!}--}}
{{--<script src="{{ asset('js/app.js') }}" defer></script>--}}
{{--<script src="{{ asset('js/vue-front.js') }}" defer></script>--}}
{{--<script src="{{ asset('js/vue-admin.js') }}" defer></script>--}}

<!-- Fonts -->
{{--<link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}

<!-- Styles -->
{!! Asset::css() !!}
{{--<link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}

@include('layouts.browser-sync')
