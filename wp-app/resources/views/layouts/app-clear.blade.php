<?php //Assets::add('app.js'); ?>
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('layouts.head')
</head>
<body>
    <div id="app">
        <main class="main">
            @yield('content')
        </main>
    </div>

{{--{!! Assets::js() !!}--}}
{!! Asset::jsVars() !!}
{!! Asset::js() !!}

{{--{!! var_dump(config('global.variables')); !!}--}}
</body>
</html>
