/* eslint-disable */
// Require the polyfill before requiring any other modules.
// require('intersection-observer');

window.server = false;

import {TweenMax, Power2, TimelineMax, Back, Linear, gsap} from 'gsap';

import { CSSRulePlugin } from "gsap/CSSRulePlugin";

gsap.registerPlugin(CSSRulePlugin);

import Vue from 'vue';

import 'svg.js';
// import {SplitText} from './libs/SplitText';
import scrollMonitor from 'scrollmonitor';
import scrollParallax from 'scrollmonitor-parallax';


import ScrollMagic from "scrollmagic"; // Or use scrollmagic-with-ssr to avoid server rendering problems
import { ScrollMagicPluginGsap } from "scrollmagic-plugin-gsap";

ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);

// import 'gsap/ScrollToPlugin';
// import ElementUI from 'element-ui';

import axios from 'axios';
// import * as qs from 'qs';

import Slick from 'vue-slick';
Vue.component('Slick', Slick);
//
// import VueMask from 'v-mask';
// Vue.use(VueMask);

import createApp from "../front";
const {app, router, store} = createApp();

// router.push('/fff/');
router.onReady(() => {
  if (window.viewData) {
    let data =  window.viewData;

    store.commit('setter', {
      viewData: data,
      global: window.global,
      // currentLanguage: document.documentElement.lang.split('-')[0],
      currentLanguage: window.global.locales.current,
      // translations: window.translations,
      // ajaxUrl: window.ajaxurl,
      // languages: window.viewData.active_languages,
    });

    if ( data.seo_title ) {
      document.title = data.seo_title;
    }

    if ( data.seo_description ) {
      let meta = document.querySelector('meta[name=description]');

      meta.setAttribute('content', data.seo_description);
    }
  }

  let token = document.querySelector('meta[name="csrf-token"]');
  if ( token ) {
    store.commit('setter', {
      token: token.getAttribute('content'),
    });
  }

  let container = document.querySelector('#app');
  if (container) {
    app.$mount(container);
  }
});

