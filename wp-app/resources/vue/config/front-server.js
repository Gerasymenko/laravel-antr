/* eslint-disable */
// require('browser-env')();

// import nodeWindowPolyfill from "node-window-polyfill";
// nodeWindowPolyfill.register(false);

// const Window = require('window');
// const { document, window } = new Window();

// const jsdom = require('jsdom').jsdom;
//
// global.document = jsdom('');
// global.window = document.defaultView;
// window.console = global.console;
//
// Object.keys(document.defaultView).forEach((property) => {
//   if (typeof global[property] === 'undefined') {
//     global[property] = document.defaultView[property];
//   }
// });
//
// global.navigator = {
//   userAgent: 'node.js'
// };
var server = true;


import createApp from "../front";
const {app, router, store} = createApp();

/* global context */
// console.log(context);
store.commit('setter', {
  viewData: context,
  global: context.global,
  currentLanguage: context.global.locales.current,
  // translations: context.translations,
  // ajaxurl: context.ajaxurl,
});

// import render from "vue-server-renderer";
// const renderer = render.createRenderer();
// // const renderer = require('vue-server-renderer').createRenderer()
//
// renderer.renderToString(app).then(html => {
//   console.log(html)
// }).catch(err => {
//   console.error(err)
// });

// console.log(router);
// print(context.acf_options);
// print(JSON.stringify(context));

import renderToString from "vue-server-renderer/basic";

router.push(context.url);
router.onReady(() => {
  renderToString(app, (err, html) => {
    if (err) {
      throw new Error(err);
    }
    // console.log(html);

    print(html);
    // print(context);
  });
});
