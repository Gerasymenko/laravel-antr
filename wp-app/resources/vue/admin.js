import Vue from 'vue';
import router from './store/router-admin';
import store from './store/store-admin';

import './store/mixins';
import vuetify from "./store/vuetify";

import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use( CKEditor );

import draggable from 'vuedraggable';
Vue.component('draggable', draggable);

// Vue.use(draggable);
// Vue.component(draggable, draggableComponent);

// // import plugin
// import { TiptapVuetifyPlugin } from 'tiptap-vuetify';
// // don't forget to import CSS styles
// import 'tiptap-vuetify/dist/main.css'
//
// // use this package's plugin
// Vue.use(TiptapVuetifyPlugin, {
//   // the next line is important! You need to provide the Vuetify Object to this place.
//   vuetify, // same as "vuetify: vuetify"
//   // optional, default to 'md' (default vuetify icons before v2.0.0)
//   iconsGroup: 'mdi'
// });


import layout from "./pages/adminLayout";

router.beforeEach((to, from, next) => {
  // if (!isAuthenticated) next('/login')
  console.log('New route ', to);

  // else
  next();
});

// const app = new Vue({
const app = new Vue({
  // el: '#app',
  render: h => h(layout),
  // template: '<div class="vue"><router-view></router-view></div>',
  // render (h) {
  //   return h('router-view')
  // },
  router,
  store,
  vuetify,
  // draggable,
});


router.onReady(() => {
  if (window.viewData) {
    let pageData = window.viewData;
    let settings = window.settings;

    if ( settings && settings.user ) {
      store.commit('setter', {
        user: settings.user,
      });

      delete settings.user;
    }

    store.commit('setter', {
      viewData: pageData,
      settings: settings,
      // translations: window.translations,
      // ajaxUrl: window.ajaxurl,
      // languages: window.viewData.active_languages,
    });
  }


  let token = document.querySelector('meta[name="csrf-token"]');
  if ( token ) {
    store.commit('setter', {
      token: token.getAttribute('content'),
    });
  }

  let container = document.querySelector('#app');
  if (container) {
    app.$mount(container);
  }
});
