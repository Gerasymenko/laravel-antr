import Vue from 'vue';
import store from './store/store';
import router from './store/router-front';
import { sync } from 'vuex-router-sync';

import {gsap} from 'gsap';
import { CSSRulePlugin } from "gsap/CSSRulePlugin";
import { TextPlugin } from "gsap/TextPlugin";

gsap.registerPlugin(CSSRulePlugin, TextPlugin);

sync(store, router);

// import ElementUI from 'element-ui';
// import "../styles/common/_element-variables.scss"
// Vue.use(ElementUI);

// Services
import './store/translates';
import './store/mixins';
import './store/directives';


// use beforeEach route guard to set the language

router.beforeEach((to, from, next) => {
  // if (!isAuthenticated) next('/login')
  console.log('New route ', to);

  // else
  next();
});


/*
router.beforeEach((to, from, next) => { // eslint-disable-line
  // use the language from the routing param or default language
  let language = to.params.lang;
  if (!language) {
    language = 'en';
  }

  // set the current language for vuex-i18n. note that translation data
  // for the language might need to be loaded first
  // Vue.i18n.set(language);
  store.commit('setter', {
    language: language,
    loading: true,
  });


  next();
});
 */


router.beforeResolve((to, from, next) => {
//   // console.log(from);
//   store.commit('setter', {
//     loading: true,
//   });

//   if (from.name && !store.state.nextRoute) {
  if (from.name) {
    store.commit('setter', {
      pageloader: true,
      nextRoute: to,
    });
    // console.log('from', from);

    store.dispatch('getPageDataTimer', to).then(() => {


      next();
    });
    // } else if (from.name && store.state.nextRoute) {
    //   store.commit('setter', {
    //     pageloader: false,
    //     nextRoute: false,
    //   });
    //
    //   next();
  } else {
    next();
  }
});

router.afterEach((to) => {
  if ( Object.keys(store.state.newData).length ) {
    // store.commit('setter', {
    //   viewData: store.state.newData
    // });
    //
    //
    // let item = store.state.viewData.item;
    // if ( item && document ) {
    //   document.body.classList.remove('active-footer');
    //   document.body.classList.remove('active-help');
    //   document.querySelector('.page').removeAttribute('style');
    //
    //   let title = item.post_title + ' - ' + store.state.viewData.site_name;
    //   console.log(item);
    //   document.title = title;
    // }

    store.dispatch('setupNewData').then(()=>{
      let item = store.state.viewData.item;
      if ( document ) {
        // if ( to.meta.disableFooter ) {
        //
        // }

        document.body.classList.remove('active-footer');
        document.body.classList.remove('active-help');
        // document.querySelector('.page').removeAttribute('style');

        if ( item.seo_title ) {
          document.title = item.seo_title;
        }

        if ( item.seo_description ) {
          let meta = document.querySelector('meta[name=description]');

          meta.setAttribute('content', item.seo_description);
        }
      }
    });
  }
});

// router.beforeResolve((to, from, next) => {
//   store.commit('setter', {
//     loading: true,
//   });
//
//   next();
// });

// Main vue
import Layout from './pages/layout';
const app = new Vue({
  router,
  store,
  render: h => h(Layout)
});

export default () => { return {app, store, router} };
