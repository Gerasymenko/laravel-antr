// window.Vue = require('vue');
import Vue from 'vue';
import router from './store/router';
import store from './store/store';

// import vuetify from "./store/vuetify";

// Vue.component('dashboard', require('./components/layout.vue').default);
// import app from "./components/app";

router.beforeEach((to, from, next) => {
  // if (!isAuthenticated) next('/login')
  console.log(to);

  // else
    next();
});

// const app = new Vue({
new Vue({
  el: '#app',
  // render: h => h(app),
  template: '<div class="vue"><router-view></router-view></div>',
  // render (h) {
  //   return h('router-view')
  // },
  router,
  store,
  // vuetify,
});
