import Vue from 'vue';
import Vuex from 'vuex';

import axios from 'axios';
import * as qs from 'qs';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    nextRoute: false,
    drawer: false,
    viewData: {},
    newData: {},
    settings: {},
    token: '',
    user: false,
    statuses: [
      {
        text: 'Publish',
        value: 'publish'
      },
      {
        text: 'Draft',
        value: 'draft'
      },
    ],
    snackbar: {
      text: '',
      color: 'success',
    },
    showSnackbar: false,
  },
  mutations: {
    setter(state, object) {
      Object.entries(object).forEach(([key, value]) => {
        Vue.set(state, key, value);
      });
    },
    setSettings(state, object) {
      Object.entries(object).forEach(([key, value]) => {
        Vue.set(state.settings, key, value);
      });
    }
  },
  actions: {
    notify({commit}, data){
      commit('setter', {
        snackbar: data,
        showSnackbar: true,
      });
    },
    togglePanel({state, commit}) {
      let drawer = !state.drawer;

      commit('setter', {
        drawer: drawer,
      });
    },
    sendRequest({state}, request) {
      axios.defaults.headers.common['Authorization'] = state.token;
      axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

      let def = {
        method: 'post',
        url: '',
        data: {}
      };


      let options = Object.assign(def, request);
      // console.log('request', request);
      if (request.data) {
        options.data = qs.stringify(request.data);
      }


      console.log('request', options);
      return axios(options);
    },
    getPageData({commit, dispatch}, slug) {
      return new Promise(function (resolve) {
        let data = {
          action: 'getPageFields',
          page: slug,
        };

        // console.log('data', data);
        dispatch('sendRequest', data).then(res => {
          console.log('res', res);

          if ( res.data.success ) {
            commit('setter', {
              newData: res.data.data
            });
          }

          resolve(res)
        });
      });
    },
  },
  getters: {

  }
});
