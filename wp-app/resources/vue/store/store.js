import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import * as qs from 'qs';

/* global axios */
export default new Vuex.Store({
  state: {
    defaultLanguage: 'en',
    currentLanguage: 'ru',
    preloader: true,
    pageloader: false,
    nextRoute: false,
    documentHeight: document ? document.documentElement.offsetHeight : 0,
    viewData: {},
    newData: {},
    settings: {},
    token: '',
    global: {},
    windowWidth: window ? window.innerWidth : 1200,
  },
  mutations: {
    setter(state, object) {
      Object.entries(object).forEach(([key, value]) => {
        Vue.set(state, key, value);
      });
    },
  },
  actions: {
    notify({commit}, data){
      commit('setter', {
        snackbar: data,
        showSnackbar: true,
      });
    },
    togglePanel({state, commit}) {
      let drawer = !state.drawer;

      commit('setter', {
        drawer: drawer,
      });
    },
    sendRequest({state}, request) {
      axios.defaults.headers.common['Authorization'] = state.token;
      axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

      let def = {
        method: 'post',
        url: '',
        data: {}
      };

      let options = Object.assign(def, request);
      // console.log('request', request);
      if (request.data && options.method !== 'get') {
        request.data = qs.stringify(request.data);
      }

      console.log(options);
      return axios(options);
    },
    getPageData({commit, dispatch, state}, next) {
      let locale = state.global.locales.current;
      let name = false;

      if ( next.meta.type === 'work' ) {
        name = next.params.slug;
      } else {
        name = next.name.toLowerCase();
      }


      return new Promise(function (resolve) {
        let url = `/api/${next.meta.type}/${name}/${locale}/`;

        let data = {
          method: 'get',
          url: url,
        };

        // console.log('data', data);
        dispatch('sendRequest', data).then(res => {
          console.log('res', res);

          if ( res.data.success ) {
            commit('setter', {
              newData: res.data.data
            });

            resolve(res)
          }
        });
      });
    },
    // getPageDataTimer({dispatch}, slug) {
    getPageDataTimer({dispatch}, next) {
      return Promise.all( [dispatch('getPageData', next), dispatch('promiseTimer')]);
      // return Promise.all( [dispatch('promiseTimer')]);
    },
    promiseTimer() {
      return new Promise(function (resolve) {
        setTimeout(()=>{
          resolve();
        }, 800);
      });
    },
    setupNewData({commit, state}) {
      commit('setter', {
        viewData: state.newData
      });
      //
      //
      // let item = state.newData.item;
      // if ( item && document ) {
      //   document.body.classList.remove('active-footer');
      //   document.body.classList.remove('active-help');
      //   document.querySelector('.page').removeAttribute('style');
      //
      //   let title = item.post_title + ' - ' + store.state.viewData.site_name;
      //   console.log(item);
      //   document.title = title;
      // }
    }
  },
  getters: {
    pageReady(state) {
      return (!state.preloader && !state.pageloader);
    },
    scroll(state) {
      let scrollPercent = 0;
      if (window) {
        let scrollTop = $(window).scrollTop();
        let docHeight = state.documentHeight;
        let winHeight = $(window).height();

        scrollPercent = (scrollTop) / (docHeight - winHeight);
      }

      // console.log(scrollPercent);

      return scrollPercent;
    },
    settings(state) {
      return state.global.settings;
    },
    item(state) {
      return state.viewData.item;
    },
    items(state) {
      return state.viewData.items;
    },
    next(state) {
      return state.viewData.next;
    }
  }
});
