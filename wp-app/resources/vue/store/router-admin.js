import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

// Pages
import Login from '../components/auth/Login';
import Registration from '../components/auth/Registration';
import Restore from '../components/auth/Restore';

import dashboard from '../pages/admin/dashboard';
import pages from '../pages/admin/Page/pages';
import work from '../pages/admin/Work/work';
import general from '../pages/admin/Settings/General';
import translates from '../pages/admin/Settings/Translates';
import singlePage from "../pages/admin/Page/singlePage";
import singleUser from "../pages/admin/Users/SingleUser";
import singleWork from "../pages/admin/Work/singleWork";
import users from "../pages/admin/Users/Users";
import menu from "../pages/admin/Menu/menu";

const router = new VueRouter({
  mode: 'history',
  base: '',
  routes: [
    {
      path: '/admin',
      name: 'Admin',
      // component: dashboard,
      component: {template: '<router-view></router-view>'},
      pathToRegexpOptions: {strict: true},
      redirect: {name: 'Dashboard'},
      children: [
        {
          path: 'dashboard/',
          name: 'Dashboard',
          component: dashboard,
          pathToRegexpOptions: {strict: true},
          meta: {
            icon: 'mdi-view-dashboard',
            menu: true,
          }
        },
        {
          path: 'pages/',
          name: 'Pages',
          component: pages,
          pathToRegexpOptions: {strict: true},
          meta: {
            icon: 'mdi-file-document-box-multiple-outline',
            menu: true,
          }
        },
        {
          path: 'work/',
          name: 'Work',
          component: work,
          pathToRegexpOptions: {strict: true},
          meta: {
            icon: 'mdi-note-text',
            menu: true,
          },
        },
        {
          path: 'users/',
          name: 'Users',
          component: users,
          pathToRegexpOptions: {strict: true},
          meta: {
            icon: 'mdi-face',
            menu: true,
          },
        },
        {
          path: 'single/user/:id?/',
          name: 'Single User',
          component: singleUser,
          pathToRegexpOptions: {strict: true},
          props: true,
        },
        {
          path: 'single/page/:id?/',
          name: 'Single Page',
          component: singlePage,
          pathToRegexpOptions: {strict: true},
          props: true,
        },
        {
          path: 'single/work/:id?/',
          name: 'Single Work',
          component: singleWork,
          pathToRegexpOptions: {strict: true},
          props: true,
        },
        {
          path: 'settings/',
          name: 'Settings',
          component: {template: '<router-view></router-view>'},
          pathToRegexpOptions: {strict: true},
          meta: {
            icon: 'mdi-settings',
            menu: true,
          },
          children: [
            {
              path: 'general/',
              name: 'General',
              component: general,
              pathToRegexpOptions: {strict: true},
              meta: {
                icon: 'mdi-yelp',
                menu: true,
              },
            },
            {
              path: 'translates/',
              name: 'Translates',
              component: translates,
              pathToRegexpOptions: {strict: true},
              meta: {
                icon: 'mdi-transcribe',
                menu: true,
              },
            },
            {
              path: 'menu/',
              name: 'Menu',
              component: menu,
              pathToRegexpOptions: {strict: true},
              meta: {
                icon: 'mdi-menu',
                menu: true,
              },
            }
          ]
        },
      ],
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      // redirect: {name: "Dashboard"}
    },
    {
      path: '/register',
      name: 'Registration',
      component: Registration,
      // redirect: {name: "Dashboard"}
    },
    {
      path: '/password/reset',
      name: 'restore',
      component: Restore,
      // redirect: {name: "Dashboard"}
    },
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return {x: 0, y: 0}
    }
  }
});

export default router;
