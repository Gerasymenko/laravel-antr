import Vue from 'vue';
// import store from './store';

Vue.mixin({
  data() {
    return {
      // translations: store.state.global.translates,
    };
  },
  computed: {
    translations() {
      return this.$store.state.global.translates;
    }
  },
  methods: {
    Translate(str) {
      let key = str.toLowerCase().replace(' ', '_');
      let translation = this.translations ? this.translations : this.$store.state.global.translates;
      let data = translation.filter(e => e.key === key);

      if ( data.length ) {
        return data[0].value;
      } else {
        return this.formatTranslationKey(str);
      }
    },
    ucFirst(text) {
      return text.charAt(0).toUpperCase() + text.slice(1);
    },
    formatTranslationKey(str) {
      let words = str.split(/[\-\_]/);

      return words.map((word, ind)=>{
        if (!ind) {
          return this.ucFirst(word);
        } else {
          return word;
        }
      }).join(' ');
    },
  },
  beforeCreate() {
    this.$options.filters.translate = this.$options.filters.translate.bind(this)
  },
  filters: {
    translate: function (value) {
      if (!value) {
        return '';
      }

      return this.Translate(value);
    }
  }
});
