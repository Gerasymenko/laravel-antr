import Vue from 'vue';
import VueRouter from 'vue-router';
import store from './store';

Vue.use(VueRouter);

// Pages
// import layout from '../pages/layout';
import home from '../pages/home';
import about from '../pages/about';
import work from '../pages/work';
import single from '../pages/single';
import services from '../pages/services';
import contacts from '../pages/contacts';
import error from '../pages/error';

let base;

// let lang = document ? document.documentElement.lang.split('-')[0] : store.state.currentLanguage;
let lang = document ? document.documentElement.lang.split('-')[0] : global.currentLocale;
// let lang = 'ru';

if ( lang == store.state.defaultLanguage ) {
  base = '/';
} else if ( lang =='uk' ) {
  base = '/ua/';
} else {
  base = `/${lang}/`;
}

let server = window ? window.server : global.server;
const router = new VueRouter({
  mode: 'history',
  base: server ? '' : base,
  routes: [
    {
      path: (server ? base : '/'),
      name: 'Home',
      component: home,
      pathToRegexpOptions: {strict: true},
      meta: {
        type: 'page',
      }
    },
    {
      path: (server ? base : '/') + 'about/',
      name: 'About',
      component: about,
      pathToRegexpOptions: {strict: true},
      meta: {
        menu: true,
        type: 'page',
      },
    },
    {
      path: (server ? base : '/') + 'work/',
      name: 'Work',
      component: work,
      pathToRegexpOptions: {strict: true},
      meta: {
        menu: true,
        type: 'page',
      },
    },
    {
      path: (server ? base : '/') + 'work/:slug/',
      name: 'Single',
      component: single,
      pathToRegexpOptions: {strict: true},
      meta: {
        type: 'work',
      },
    },
    {
      path: (server ? base : '/') + 'services/',
      name: 'Services',
      component: services,
      pathToRegexpOptions: {strict: true},
      meta: {
        menu: true,
        type: 'page',
      },
    },
    {
      path: (server ? base : '/') + 'contacts/',
      name: 'Contacts',
      component: contacts,
      pathToRegexpOptions: {strict: true},
      meta: {
        menu: true,
        type: 'page',
        disableFooter: true,
      },
    },
    {
      path: (server ? base : '/') + '*',
      component: error,
      name: 'Page404',
      meta: {
        disableFooter: true,
      }
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
    // document.body.classList.remove('active-help');
      return {x: 0, y: 0}
    }
  }
});

export default router;
