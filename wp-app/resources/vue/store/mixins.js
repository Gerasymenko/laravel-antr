import Vue from 'vue';
import {TweenMax, Linear} from 'gsap';
// import store from "./store";

Vue.mixin({
  data() {
    return {
      // translates: window.site_setting.setting.translates,
    };
  },
  computed: {
    globalLocale() {
      // console.log(this.$store.state);
      if (this.$store.state.global) {
        return this.$store.state.global.locales.current;
      } else {
        return false;
      }
    },
    dateFormat() {
      if ( this.$store.state.global ) {
        let settings = this.$store.state.global.settings;

        return this.getDataByKey(settings, 'date_format').value;
      } else {
        return false;
      }
    }
  },
  methods: {
    isEmpty(obj) {
      if (obj.hasOwnProperty('length')) {
        return obj.length;
      } else {
        return Object.keys(obj).length;
      }
    },
    getDataByKey(object, key) {
      let data = object.filter(e => e.key === key);
      return data[0];
    },
    getValuseByKey(object, key) {
      let data = object.filter(e => e.key === key);
      return data;
    },
    getDataByGroup(object, group, filter = false) {
      let data = object.filter(e => e.group === group);

      if ( !filter ) {
        return data;
      } else if ( filter === 'key=value' ) {
        return this.toKeyValue(data);
      }

    },
    initParallax(el) {
      if (window && window.innerWidth < 1200) {
        return;
      }

      setTimeout(() => {
        let offsetY = el.getAttribute('data-parallax') ? el.getAttribute('data-parallax') : 50;
        let scrollParallax = require('scrollmonitor-parallax');

        // Create a root element. Parallax will start when this
        // element enters the viewport and stop when it exits.
        let parallaxRoot = scrollParallax.create(el);
        // to make an element scroll at a speed relative to the
        // scroll parent, just add a value for speed.
        // let parallaxChild2 = parallaxRoot.add(domElement, 0.5);

        // for more complex animations you can start and end positions.
        // If it's left blank, the start position is taken from the element's CSS.
        parallaxRoot.add(
          el, {
            start: {
              // opacity: 0
            },
            end: {
              // x: 100,
              y: offsetY,
              // z: 100,
              // opacity: 0.7
            },
            easing: {
              // y: eases.linear,
              y: Linear.easeNone,
              // y: Power2.easeOut,
              // z: eases.circIn,
              // opacity: eases.bounceIn
            }
          }
        );
      }, 500);
    },
    getScroll() {
      let scrollTop = $(window).scrollTop();
      let docHeight = document.documentElement.offsetHeight;
      let winHeight = $(window).height();

      let scrollPercent = (scrollTop) / (docHeight - winHeight);
      console.log(scrollPercent);

      return scrollPercent;
    },
    setAnimateEl(el) {
      if (!el) {
        return;
      }
      TweenMax.set(el, {yPercent: 25, scaleY: 1.35, transformOrigin: "50% 0", opacity: 0});
    },
    animateEl(el, delay = 0) {
      if (!el) {
        return;
      }
      TweenMax.to(el, .5, {yPercent: 0, scaleY: 1, transformOrigin: "50% 0", opacity: 1, ease: "none", delay: delay});
    },
    showEl(el, delay = 0) {
      if (!el) {
        return;
      }

      TweenMax.to(el, .5, {opacity: 1, ease: "none", delay: delay});
    },
    hideEl(el, delay = 0) {
      if (!el) {
        return;
      }

      TweenMax.to(el, .5, {opacity: 0, ease: "none", delay: delay});
    },
    getNum(index) {
      index += 1;
      return index.toString().padStart(2, '0');
    },
    parseJson(content) {
      try {
        return JSON.parse(content)
      } catch (e) {
        console.log('error', e);
        return false;
      }
    },
    toKeyValue(obj) {
      let new_object = {};

      Object.values(obj).forEach(item=>{
        new_object[item.key] = item.value;
      });

      return new_object;
    },
    nl2br (str, replaceMode = 1, isXhtml = 1) {

      var breakTag = (isXhtml) ? '<br />' : '<br>';
      var replaceStr = (replaceMode) ? '$1'+ breakTag : '$1'+ breakTag +'$2';
      return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, replaceStr);
    },
    FormatDate(value, format = false) {
      let moment = require('moment');
      moment.locale(this.globalLocale);

      format = format ? format : this.dateFormat;

      return moment(value).format(format);
    }
  },
  beforeCreate() { // Before create action
    // Vue.prototype.$filters = {};

    Object.keys(Vue.options.filters).forEach(name => {
      this.$options.filters[name] = this.$options.filters[name].bind(this);
      // Vue.prototype.$filters[name] = this.$options.filters[name].bind(this);
    });
  },
  filters: {
    capitalize(value) {
      if (!value) {
        return '';

      }
      value = value.toString();

      return value.charAt(0).toUpperCase() + value.slice(1);
    },
    ucFirst(text) {
      return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
    },
    formatDate(value, format = false) {
      return this.FormatDate(value, format);
    }
  },
});

Vue.prototype.$filters = Vue.options.filters;
