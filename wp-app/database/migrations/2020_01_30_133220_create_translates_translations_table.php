<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslatesTranslationsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create( 'translates_translations', function ( Blueprint $table ) {
      $table->increments( 'id' );
      $table->string( 'locale' );
      $table->text( 'value' )->nullable();
      $table->bigInteger( 'translates_id' )->unsigned();
      $table->foreign( 'translates_id' )
        ->references( 'id' )
        ->on( 'translates' )
        ->onDelete( 'cascade' );
      $table->timestamps();
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public
  function down() {
    Schema::dropIfExists( 'translations' );
  }
}
