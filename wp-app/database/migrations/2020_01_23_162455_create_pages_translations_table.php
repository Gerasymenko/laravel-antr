<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_translations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('locale');
          $table->string('title')->nullable();
          $table->string('seo_title')->nullable();
          $table->text('seo_description')->nullable();
          $table->text('description')->nullable();
          $table->text('content')->nullable();
          $table->bigInteger('page_id')->unsigned();
          $table->foreign('page_id')
            ->references('id')
            ->on('pages')
            ->onDelete('cascade');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages_translations');
    }
}
