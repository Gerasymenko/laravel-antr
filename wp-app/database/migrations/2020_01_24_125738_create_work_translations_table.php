<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_translations', function (Blueprint $table) {
          $table->increments('id');
          $table->string('locale');
          $table->string('title')->nullable();
          $table->string('seo_title')->nullable();
          $table->text('seo_description')->nullable();
          $table->text('description')->nullable();
          $table->text('content')->nullable();
          $table->bigInteger('work_id')->unsigned();
          $table->foreign('work_id')
            ->references('id')
            ->on('works')
            ->onDelete('cascade');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_translations');
    }
}
