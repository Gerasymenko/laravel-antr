<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create( 'settings', function ( Blueprint $table ) {
      $table->bigIncrements( 'id' );
      $table->text( 'type' )->nullable();
      $table->string( 'group' )->nullable();
      $table->string( 'key' )->nullable();
      $table->string( 'value' )->nullable();
      $table->string( 'title' )->nullable();
      $table->integer( 'order' )->default(0);
      $table->timestamps();
    } );
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists( 'settings' );
  }
}
