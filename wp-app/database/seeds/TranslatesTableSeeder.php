<?php

use Illuminate\Database\Seeder;

class TranslatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      App::setLocale('en');
      factory(App\Translates::class, 1)->create();
    }
}
