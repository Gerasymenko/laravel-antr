<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Translates;
use Faker\Generator as Faker;

$factory->define(Translates::class, function ( Faker $faker) {
    $title = $faker->sentence(2);
    $slug = str_replace( '-','_',$faker->slug(2));

    return [
        'key' => $slug,
        'value' => $title,
    ];
});
