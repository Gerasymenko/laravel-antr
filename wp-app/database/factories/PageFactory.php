<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Page;
use Faker\Generator as Faker;

$factory->define(Page::class, function ( Faker $faker) {
    $title = $faker->sentence;
    $desc = $faker->paragraph;

    return [
        'title' => $title,
        'seo_title' => $title,
        'slug' => Transliterate::slugify($title),
//        'slug' => $faker->word,
//        'slug' => '',
        'description' => $desc,
        'seo_description' => $desc,
        'content' => $faker->randomHtml,
        'status' => 'publish',
    ];
});
