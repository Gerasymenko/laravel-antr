<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Work;
use Faker\Generator as Faker;

$factory->define(Work::class, function ( Faker $faker) {
    $title = $faker->sentence;
    $desc = $faker->paragraph;

    return [
        'title' => $title,
        'seo_title' => $title,
        'slug' => Transliterate::slugify($title),
        'description' => $desc,
        'seo_description' => $desc,
        'content' => $faker->randomHtml,
        'status' => 'publish',
    ];
});
