<?php

return [
    /*
     * The list of locales that are supported by the application.
     * This can be overwritten by setting a 'app.locales'.
     */
    'supported' => [
        'en',
    ],
];
