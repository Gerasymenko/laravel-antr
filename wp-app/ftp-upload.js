const WebpackSftpClient = require('webpack-ftp-upload-plugin');
const atob = require('atob');
const gitRoot = require('git-root');
const path = require('path');

module.exports = function (PATHS, folder) {
  let ftp_project_dir = process.env.PROJECT_NAME + '/www/';
  // var relPath = gulpPath.toRelative(process.env.PROJECT_NAME, distPath);
  let relPath = path.relative(gitRoot(), __dirname);
  relPath = path.join(relPath, './public').replace(/\\/g, '/');

  var ftp_options = {
    host: process.env.FTP_HOST,
    port: process.env.FTP_PORT,
    username: process.env.FTP_USER,
    password: atob(process.env.FTP_PASS),
    local: PATHS.dist + folder,
    path: process.env.FTP_PATH + ftp_project_dir + relPath + folder,
  };

  // console.log(ftp_options);

  return {
    plugins: [
      new WebpackSftpClient(ftp_options),
    ]
  }
};
