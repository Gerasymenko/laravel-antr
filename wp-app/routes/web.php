<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::multilingual('/', 'PagesController@index')->name('home');
//Route::multilingual('/page/{name}', 'PagesController@AjaxPageData');

//Route::get('/about', 'HomeController@index')->name('about');
//Route::get('/work', 'PagesController@index')->name('work');
//Route::get('/single', 'HomeController@index')->name('single');
//
//Route::get('/services', 'HomeController@index')->name('services');
//Route::get('/contacts', 'HomeController@index')->name('contacts');
//
//Route::any('/test', 'HomeController@test')->name('test');

Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', 'AdminController@index')->name('admin');
    Route::post('logout', 'AdminController@logout')->name('admin/logout');
    Route::get('{slug?}', 'AdminController@index')->name('admin');
    Route::any('/{any}', 'AdminController@index')->where('any', '.*');
//    Route::get('page/{id}/', 'PagesController@page');
//    Route::get('pages', 'PagesController@index');
});

Route::get('/password/reset', 'Auth\ResetPasswordController@index');
Route::post('/send-email/', 'EmailController@sendEmail');

// Ajax methods
Route::post('/restore', 'Auth\ResetPasswordController@AjaxRestore')->name('restore');
Route::post('/registration', 'Auth\RegisterController@AjaxRegistration')->name('registration');
Route::post('/login/authenticate', 'Auth\LoginController@authenticate')->name('login/authenticate');
Route::post('/getPageData', 'Controller@getPageData');


//Route::multilingual('/{any}', 'PagesController@index')->name('404');
