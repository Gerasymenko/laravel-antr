<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware( 'auth:api' )->get( '/user', function ( Request $request ) {
  return $request->user();
} );


$routes = [
  'pages'       => 'Api\PagesController',
  'works'       => 'Api\WorkController',
  'translatess' => 'Api\TranslatesController',
  'settingss'   => 'Api\SettingsController',
  'users'      => 'Api\UsersController',
  'menus'      => 'Api\MenuController',
];

foreach ( $routes as $key => $class ) {
  //$item = $key === 'pages' ? substr($key, 0, strlen($key)-1) : $key;
  $item = substr( $key, 0, strlen( $key ) - 1 );
  Route::get( '/' . $key, "{$class}@index" );
  Route::post( '/' . $key, "{$class}@store" );
  Route::post( '/' . $key . '/{' . $item . '}/upload/', "{$class}@uploadTo" );
  Route::get( '/' . $key . '/{' . $item . '}/{locale?}', "{$class}@show" );
  Route::put( '/' . $key . '/{' . $item . '}', "{$class}@update" );
  Route::delete( '/' . $key . '/{' . $item . '}', "{$class}@destroy" );
}
Route::get('/page/{name}/{locale?}/', 'PagesController@AjaxPageData');
Route::get('/work/{name}/{locale?}/', 'WorksController@AjaxPageData');

Route::any( '/{any}', function ( $any, Request $request ) {
  $namespace = '\App\Http\Controllers\Api\ApiController';
  $path = explode( '/', $any );
  $method = $path[0];
  $parameters = [];

  if ( count( $path ) > 1 ) {
    $parameters[]['id'] = $path[1];
  }

  if ( method_exists( $namespace, $method ) ) {
    return \App::call( "{$namespace}@{$method}", $parameters );
  } else {
    return \App::call( "{$namespace}@index", $parameters );
  }
} )->where( 'any', '.*' );
