const mix = require('laravel-mix');

const path = require('path');

const DotEnv = require('webpack-dotenv-plugin');
const FtpUpload = require('./ftp-upload');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// let productionSourceMaps = false;
let productionSourceMaps = process.env.NODE_ENV !== 'production';

let PATHS = {
  resources: path.join(__dirname, './resources'),
  js: path.join(__dirname, './resources/js'),
  scss: path.join(__dirname, './resources/sass'),
  dist: path.join(__dirname, './public'),
};

mix.webpackConfig({
  resolve: {
    alias: {
      resources: PATHS.resources,
      js: PATHS.js,
      scss: PATHS.scss,
    }
  },
  plugins: [
    new DotEnv({
      sample: './.env.example',
      path: './.env',
      allowEmptyValues: true,
    }),
  ],
});


if (process.env.NODE_ENV === 'ssr') {
  // mix.js('resources/js/app.js', 'public/js')
  //   .sass('resources/sass/app.scss', 'public/css')
  //   .sourceMaps(productionSourceMaps, 'source-map');

  mix.js('resources/vue/config/front-server.js', 'public/js/server-vue.js')
    .options({
      extractVueStyles: "public/css/server-vue.css"
    })
    .sourceMaps(productionSourceMaps, 'source-map');
} else if (process.env.NODE_ENV === 'admin') {
  mix.js('resources/vue/admin.js', 'public/js/vue-admin.js')
    // .options({
    //   extractVueStyles: "public/css/vue-admin.css"
    // })
    .sourceMaps(productionSourceMaps, 'source-map')
    // .then((stats) => {
    //   console.log(stats);
    // })
  ;

  mix.webpackConfig( FtpUpload(PATHS, '/css/'));
  mix.webpackConfig(FtpUpload(PATHS, '/js/'));
} else {
  mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sourceMaps(productionSourceMaps, 'source-map');

  mix.js('resources/vue/config/front-client.js', 'public/js/vue-front.js')
    .options({
      extractVueStyles: "public/css/vue-front.css"
    })
    // .js('resources/vue/admin.js', 'public/js/vue-admin.js')
    // .sass('resources/vue/config/front-client.js', 'public/css')
    .sourceMaps(productionSourceMaps, 'source-map');

  mix.copyDirectory('resources/images', 'public/images');

  mix.webpackConfig( FtpUpload(PATHS, '/css/'));
  mix.webpackConfig(FtpUpload(PATHS, '/js/'));
}



mix.browserSync({
  // proxy: 'http://wp.docker.localhost:8000/'
  port: 4000,
  reloadDelay: 200,
  target: 'http://wp.docker.localhost:8000/',

  host: null,
  proxy: false,
});
