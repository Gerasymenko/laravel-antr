<?php

namespace App;

use App\Providers\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Support\Facades\App;
use Transliterate;

class Work extends Model implements HasMedia {

  use HasMediaTrait;
  use TranslatableTrait;


  public $translatedAttributes = ['title', 'content', 'description', 'seo_title', 'seo_description'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'slug', 'status'
  ];

//    public function find( $id ) {
//        return $id;
//    }

  /**
   * Change updated date format
   * @param $value
   * @return false|string
   */
  public function getUpdatedAtAttribute( $value ) {
    $format = 'd.m.Y H:i';

    return date( $format, strtotime( $value ) );
//        return $value;
  }

  /**
   * Register collection
   */
  public function registerMediaCollections() {
    $this
      ->addMediaCollection( 'thumbnail' )
      ->singleFile();
  }

  public function setSlugAttribute($value) {
    if ( empty($value) || !isset($value) ) {
      $this->attributes['slug'] = Transliterate::slugify($this->title);
    } else {
      $this->attributes['slug'] = $value;
    }
  }

  public function getContentAttribute($value) {
    if ( is_string($value) ) {
      return json_decode($value);
    }

    return $value;
  }

//  public function translation($language = null)
//  {
//    if ($language == null) {
//      $language = App::getLocale();
//    }
//    return $this->hasMany('App\WorkTranslation')->where('language', '=', $language);
//  }

}
