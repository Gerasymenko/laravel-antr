<?php

namespace App\Http\Controllers;

//use Fisharebest\LaravelAssets\Assets;
use App\Http\Middleware\GlobalSettings;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
//use Illuminate\Routing\Route;
use Route;
use Spatie\Ssr\Engines\V8;
use Spatie\Ssr\Renderer;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $layout = 'layouts.master';

    public function index() {
//        Assets::add('app.js');

//        $this->layout->vars = 'test';
    }

    public static function addAsset($file) {
        \Asset::add($file);
    }

    public static function addJsVar($vars, $group = false ) {
      if ($group) {
        \Asset::add($vars, 'var', $group);
      } else {
        \Asset::add($vars, 'var');
      }
    }

//    public function getPageData(Request $request) {
//        $r = $request->input('route');
////        var_dump($r);
////        $router = new Route();
//
//        $controller = self::getControllerByRouteName($r);
//
////        Route::getBindingCallback($r);
////        var_dump( $controller );
//
//        return response()->json( ['success' => true, 'data' => $controller::getData()] );
//    }

    public static function getControllerByRouteName($name) {
        $routes = Route::getRoutes()->getRoutesByName();

        if ( isset($routes[$name]) ) {
            $controller = $routes[$name]->action['controller'];

//            return $routes[$name];
            return explode('@', $controller)[0];
        }

        return false;
    }

    public static function ssr($data) {
      $html = false;

      if ( class_exists('V8Js') ) {
        $v8 = new \V8Js();
        $engine = new V8( $v8 );
        $renderer = new Renderer( $engine );

        $data['url'] = $_SERVER['REQUEST_URI'];
        $data['global'] = GlobalSettings::getGlobalSettings();

        $path = public_path( 'js/server-vue.js' );

        $v8->executeString( 'var process = { env: { VUE_ENV: "server", NODE_ENV: "production" }}; this.global = { process: process };' );
        $v8->executeString( 'var document, window = undefined;' );
        $v8->executeString( 'setTimeout = function() { return false; };' );
        $v8->executeString( 'clearTimeout = function() { return false; };' );

        $locale = \Locales::current();
        $default = config( 'app.fallback_locale' );
        $v8->executeString( "var currentLocale = '{$locale}';" );
        $v8->executeString( "var defaultLocale = '{$default}';" );
        $v8->executeString( "var server = true;" );

//        var_dump($locale);;
//        die();

//        $renderer->debug( true );

        $renderer->context( $data );
        $renderer->entry( $path );

        $html = $renderer->render();
      }

      return $html;
    }

    public static function debug($var) {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
    }
}
