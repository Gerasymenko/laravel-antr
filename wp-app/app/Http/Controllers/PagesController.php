<?php

namespace App\Http\Controllers;

use App\Http\Resources\PageResource;
use App\Http\Resources\TranslatesCollection;
use App\Http\Resources\WorkListResource;
use App\Http\Resources\WorkResource;
use App\Page;
use App\Providers\Classes\Locales;
use App\Settings;
use App\Translates;
use App\Work;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;

class PagesController extends Controller {

  public function __construct() {
    $this->middleware( 'global' );
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index() {
    $front_page = Settings::where( [ 'key' => 'front_page' ] )->first()->value;

    self::setPageData( $front_page );

    $data = self::getData($front_page);
    $html = self::ssr($data);

    return view( 'home', ['html' => $html] );
  }

  public function about() {
    self::setPageData();

    $data = self::getData();
    $html = self::ssr($data);

    return view( 'home', ['html' => $html] );
  }

  public function work() {
    self::setPageData();

    $data = self::getData();
    $html = self::ssr($data);

    return view( 'home', ['html' => $html] );
  }

  public function services() {
    self::setPageData();

    $data = self::getData();
    $html = self::ssr($data);

    return view( 'home', ['html' => $html] );
  }

  public function contacts() {
    self::setPageData();

    $data = self::getData();
    $html = self::ssr($data);

    return view( 'home', ['html' => $html] );
  }


  public static function getData( $name = false ) {
    $name = $name ?: self::getSelfName( 2 );
    $page = Page::where( [ 'slug' => $name ] )->first();
    $resources = new PageResource( $page );
    $works = false;

    $data = [
      'item' => $resources->toArray(),
    ];

    if ( $name === 'home' ) {
      $works = Work::where( [ 'status' => 'publish' ] )->paginate( 6 );
    } elseif ( $name === 'work' ) {
      $works = Work::where( [ 'status' => 'publish' ] )->get();
    }

    if ( $works ) {
      $data['items'] = WorkResource::collection( $works );
    }


    return $data;
  }

  public function AjaxPageData( $name, $locale ) {
//    $fields = $request->all();
    \App::setLocale( $locale );

    $data = self::getData( $name );

    return response()->json( [ 'success' => true, 'data' => $data ] );
  }

  public static function setPageData( $name = false ) {
    $name = $name ?: self::getSelfName( 2 );

    $page = Page::where( [ 'slug' => $name ] )->first();
    if ( isset( $page->seo_title ) ) {
      \Config::set( 'app.name', $page->seo_title );
    }
    if ( isset( $page->seo_description ) ) {
      \Config::set( 'app.description', $page->seo_description );
    }


    self::addJsVar( self::getData( $name ) );
  }

  public static function getSelfName( $step = 1 ) {
    $trace = debug_backtrace();

    return $trace[ $step ]["function"];
  }

  public static function debug( $var ) {
    echo "<pre>";
    var_dump( $var );
    echo "</pre>";
  }
}
