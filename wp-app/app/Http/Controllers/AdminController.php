<?php


namespace App\Http\Controllers;


use App\Helpers\AdminSettings;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use Route;
use Sichikawa\LaravelSendgridDriver\Transport\SendgridTransport;
use Locales;

class AdminController extends Controller {
  use AuthenticatesUsers;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct() {
//        \Mail::send('admin', [], function (Message $message) {
//            $message
//                ->subject('Test message')
////                ->to('foo@example.com', 'foo_name')
//                ->from('bar@example.com', 'bar_name')
//                ->to('wordpress@svitsoft.com');
////                ->embedData([
////                    'personalizations' => [
////                        [
////                            'dynamic_template_data' => [
////                                'title' => 'Subject',
////                                'name'  => 's-ichikawa',
////                            ],
////                        ],
////                    ],
//////                    'template_id' => config('services.sendgrid.templates.dynamic_template_id'),
////                ],
////                    SendgridTransport::SMTP_API_NAME);
////                ], 'sendgrid/x-smtpapi');
//        });

    self::addAsset( 'vue-admin.js' );
//  \Schema::dropIfExists('settings');
//  \Schema::dropIfExists('settings_translations');
//    self::addJsVar( self::getData(), 'settings' );

    $this->middleware( 'auth' );
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index() {
    self::addAsset( 'vue-admin.js' );
//
    self::addJsVar( self::getData(), 'settings' );

//    var_dump(self::getData());

    return view( 'admin' );
  }

  public static function getData() {
    return [
      'urls'          => AdminSettings::globalUrls(),
      'actions'       => AdminSettings::globalActions(),
      'user'          => AdminSettings::userData(),
      'currentLocale' => Locales::current(),
      'locales'       => Locales::all(),
    ];
  }

  public function logout() {
//        \Session::flush();
    Auth::logout();

    return response()->json( [ 'success' => true ] );
  }
}
