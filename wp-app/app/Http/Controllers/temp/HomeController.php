<?php

namespace App\Http\Controllers;

//use Fisharebest\LaravelAssets\Assets;
//use App\Providers\Classes\Asset;
use Illuminate\Http\Request;

class HomeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        var_dump(asset('js/app.js'));
//        \Assets::add('app.js');
//        Auth::login()
//        $this->middleware( 'auth' );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
//        \Assets::add('vue-front.js');
        self::addAsset('vue-front.js');
        self::addJsVar([
            'settings'=> \App\Settings::all(),
            'translates'=> \App\Translates::all()
        ]);

//        Assets::add
        config(['global.variables' => 'test']);

        return view( 'home' );
    }

    public function test() {
        var_dump(111);
//        var_dump($request->expectsJson());
    }
}
