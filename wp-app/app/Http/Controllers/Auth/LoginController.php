<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\AdminSettings;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as GlobalAuth;
//use Illuminate\Http\RedirectResponse as Redirect;
use Redirect;

class LoginController extends AdminController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

//    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/admin';
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        self::addAsset('vue-admin.js');

        self::addJsVar(self::getData(), 'settings');

        $this->middleware('guest')->except('logout');
    }

//    public static function getData() {
//        return [
//            'urls' => AdminSettings::globalUrls(),
//        ];
//    }

    public function authenticate(Request $request) {
        request()->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');
        if (GlobalAuth::attempt($credentials)) {
            // Authentication passed...
            $user = GlobalAuth::user();
            return response()->json( [
                'success' => true,
                'user' => $user,
            ] );
        } else {
            return response()->json( [
                'success' => false,
                'message' => 'Oops! You have entered invalid credentials',
            ] );
        }
        //return Redirect::to("login")->withSuccess('Oppes! You have entered invalid credentials');
    }
}
