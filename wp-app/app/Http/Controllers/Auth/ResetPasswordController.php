<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller {
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    public function __construct() {
        self::addAsset( 'vue-admin.js' );
        $this->middleware( 'guest' );
    }

    public function index() {
        return view( 'auth.passwords.reset' );
    }

    public function AjaxRestore( Request $request ) {
        $data = $request->all();

        $status = User::where( 'email', '=', $data['email'] )->exists();

        if ( $status ) {

            $credentials = [
                'email'    => $data['email'],
                'password' => $data['password'],
            ];

            Password::reset( $credentials );
        }
        $response = [
            'status' => $status,
        ];

        return response()->json( $response );

    }
}
