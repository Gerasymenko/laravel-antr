<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\TranslatesResource;
use App\Translates;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class TranslatesController extends Controller {

  public function index() {
    $local = $_GET['local'] ?: 'en';
    \App::setLocale( $local );
    return TranslatesResource::collection( Translates::paginate( 50 ) );
  }


  /**
   * @param Request $request
   * @return array
   */
  public function store( Request $request ) {
    $data = $request->all();

    if ( isset( $data['locale'] ) ) {
      \App::setLocale( $data['locale'] );
    }

    $fields = (isset( $data['item'] )) ? $data['item'] : $data;

    $validator = Validator::make( $fields['en'], [
      'key'   => 'required',
      'value' => 'required',
    ] );

    if ( $validator->fails() ) {
      return [
        'success' => false,
        'data'    => $fields,
      ];
    }

    /**
     * @var $page Translates
     */
    $page = Translates::create( $fields['en'] );

    $locales = \Locales::all();

    foreach ( $locales as $locale ) {
      \App::setLocale( $locale );
      $page->value = $fields[ $locale ]['value'];
      $page->update();
    }

    return [
      'success' => true,
      'item'    => new TranslatesResource( $page ),
      'locales' => $locales,
    ];
  }


  /**
   * @param Translates $translates
   * @param bool $locale
   * @return array
   */
  public function show( Translates $translates, $locale = false ) {
    $data = $translates->getTranslationsArray();
    \App::setLocale( $locale );
//
//    return new TranslatesResource( $translates );

    return $data;
  }


  /**
   * @param Request $request
   * @param Translates $translates
   * @return TranslatesResource|array
   */
  public function update( Request $request, Translates $translates ) {
    $data = $request->all();

    $fields = (isset( $data['item'] )) ? $data['item'] : $data;
    $locales = \Locales::all();

    $validator = Validator::make( $fields['en'], [
      'key' => 'required',
    ] );

    if ( $validator->fails() ) {
      return [
        'success' => false,
        'data'    => $data
      ];
    }

    foreach ( $locales as $locale ) {
      \App::setLocale( $locale );
      $translates->value = $fields[ $locale ]['value'];
      $translates->update();
    }

//    $translates->update( $fields );

    return new TranslatesResource( $translates );
  }

  /**
   * @param Translates $translates
   * @return array
   * @throws \Exception
   */
  public function destroy( Translates $translates ) {
    $translates->delete();
    return [
      'success' => true,
    ];
  }
}
