<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PageResource;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ApiController extends Controller {

  public function index() {
//        return factory('App\Pages', 10)->make();
//        return PagesResource::collection( Pages::paginate(50) );
    return [
      'success' => false,
      'message' => 'Method not found!',
    ];
  }

  public function upload( Request $request ) {
    $paths = [];

    /**
     * @var  $file UploadedFile
     */
//    $id = intval($request->get('id'));
//    $page = Page::find($id);

    foreach ( $request->files->all() as $key => $file ) {
      $origin = $file->getClientOriginalName();
      $full_path = storage_path( "app/media/{$origin}" );

      if ( !file_exists( $full_path ) ) {
        $path = $request->file( $key )->storeAs( 'media', $origin );
      } else {
        $path = $request->file( $key )->store( 'media' );
      }

      if ( $path ) {
//        self::acceptToPost($key, $path, $page);
        $paths[] = $path;
      }
    }

    return $paths;
  }

  public static function acceptToPost($name, $thumb, $page) {
    $full_path = storage_path( "app/{$thumb}" );
    $explode = explode('/', $thumb);

    if ( file_exists($full_path) && count($explode) < 3 ) {
      $page->addMedia($full_path)->toMediaCollection($name);
    }
  }

  public static function uploadToPost($post, Request $request) {
    $paths = [];

    /**
     * @var  $file UploadedFile
     */

    foreach ( $request->files->all() as $key => $file ) {
      $origin = $file->getClientOriginalName();
      $full_path = storage_path( "app/media/{$origin}" );

      if ( !file_exists( $full_path ) ) {
        $path = $request->file( $key )->storeAs( 'media', $origin );
      } else {
        $path = $request->file( $key )->store( 'media' );
      }

      if ( $path ) {
        $full_path = storage_path( "app/{$path}" );
        $explode = explode('/', $path);

        if ( file_exists($full_path) && count($explode) < 3 ) {
          $image = $post->addMedia($full_path)->toMediaCollection($key);

//          /media/${images.data.id}/${images.data.file_name}`;
          $path = "/media/$image->id/$image->file_name";
//          var_dump($image);
        }

        $paths[] = $path;
      }
    }

    return $paths;
  }


  public static function uploadToSettings($settings, Request $request) {
    /**
     * @var  $file UploadedFile
     */
    foreach ( $request->files->all() as $key => $file ) {
      $origin = $file->getClientOriginalName();
      $full_path = storage_path( "app/media/{$origin}" );

      if ( !file_exists( $full_path ) ) {
        $path = $request->file( $key )->storeAs( 'media', $origin );
      } else {
        $path = $request->file( $key )->store( 'media' );
      }
      $full_path = storage_path( "app/{$path}" );


    }

    return $settings->addMedia($full_path)->toMediaCollection($key);
  }

}
