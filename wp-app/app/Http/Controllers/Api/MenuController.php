<?php
  
  namespace App\Http\Controllers\Api;
  
  use App\Http\Resources\MenuResource;
  use App\Http\Resources\PageListResource;
  use App\Http\Resources\WorkListResource;
  use App\Menu;
  use App\Page;
  use App\Work;
  use Illuminate\Http\Request;
  
  class MenuController extends ApiController {
    public function index() {
      return [
        'menu'  => MenuResource::collection( Menu::paginate( 50 ) ),
        'pages' => PageListResource::collection( Page::paginate( 50 ) ),
      ];
    }
    
    
    public function store( Request $request ) {
      $data = $request->all();
      
      /**
       * @var $menu Menu
       */
      foreach ( $data as $item ) {
        $id    = isset( $item['id'] ) ? $item['id'] : 0;
        $is_id = Menu::where( 'id', '=', $id )->exists();
        
        if ( $is_id ) {
          Menu::where('id', $id)->update(array(
            'page_id' 	  =>  $item['page_id'],
            'page_type' =>  $item['page_type'],
            'order' =>  $item['order'],
          ));
          
        } else {
          $menu = Menu::create( $item );
          $menu->update();
        }
      }
      return MenuResource::collection( Menu::paginate( 50 ) );
    }
    
    /**
     * @param Request $request
     * @param Menu $menu
     * @return MenuResource|array
     */
    public function update( Request $request, Menu $menu ) {
      $data = $request->all();
      
      foreach ( $data as $item ) {
        $menu->update( $item );
      }
      
      return new MenuResource( $menu );
    }
    
    /**
     * @param Menu $menu
     * @return array
     * @throws \Exception
     */
    public function destroy( Menu $menu ) {
      $menu->delete();
      return [
        'success' => true,
      ];
    }
    
  }
