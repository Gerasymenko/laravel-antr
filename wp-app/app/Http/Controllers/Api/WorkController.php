<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\WorkListResource;
use App\Http\Resources\WorkResource;
use App\Work;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WorkController extends ApiController
{
  public function index() {
    return WorkListResource::collection( Work::paginate( 50 ) );
  }

  public function show( Work $work, $locale = false ) {
    \App::setLocale($locale);

    return new WorkResource( $work );
  }

  public function destroy( Work $work ) {
    $work->delete();

    return [
      'success' => true,
    ];
  }

  public function update( Work $work, Request $request ) {
    $fields = $this->getPostData($request);

    if ( !$fields ) {
      return [
        'success' => false,
      ];
    }

    $work->update( $fields );

    if (!isset($fields['thumbnail']) || empty($fields['thumbnail'])) {
      $work->clearMediaCollection('thumbnail');
    }

    return new WorkResource( $work );
  }

  public function store( Request $request ) {
    $fields = $this->getPostData($request);

    if ( !$fields ) {
      return [
        'success' => false,
      ];
    }

    $work = Work::create( $fields );
    if (isset($fields['thumbnail']) && !empty($fields['thumbnail'])) {
      $thumb = is_array($fields['thumbnail']) ? $fields['thumbnail'][0] : $fields['thumbnail'];

      $work->addMedia($thumb)->toMediaCollection('thumbnail');
    }

    return [
      'success' => true,
      'item' => new WorkResource( $work ),
    ];
  }

  public function getPostData( Request $request ) {
    $data = $request->all();

    if (isset($data['locale'])) {
      \App::setLocale($data['locale']);
    }

    $fields = ( isset($data['item']) ) ? $data['item'] : $data;

    $validator = Validator::make($fields, [
      'title' => 'required',
      'status' => 'required',
    ] );

    if ($validator->fails()) {
      return false;
    } else {
      return $fields;
    }
  }

  public function uploadTo(Work $work, Request $request) {
    return self::uploadToPost($work, $request);
  }
}
