<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\SettingsResource;
use App\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class SettingsController extends ApiController {

  public function index() {
    return SettingsResource::collection( Settings::paginate( 50 ) );
  }

  /**
   * @param Request $request
   * @return array
   */
  public function store( Request $request ) {
    $data = $request->all();

    $fields = (isset( $data['item'] )) ? $data['item'] : $data;

    $validator = Validator::make( $fields, [
      'key' => 'required',
    ] );

    if ( $validator->fails() ) {
      return [
        'success' => false,
        'data'    => $fields,
      ];
    }

    /**
     * @var $page Settings
     */
    $page = Settings::create( $fields );

    $page->update();

    return [
      'success' => true,
      'item'    => new SettingsResource( $page ),
    ];
  }

  public function update( Request $request, Settings $settings ) {

    $data = $request->all();

    $fields = (isset( $data['item'] )) ? $data['item'] : $data;

    $validator = Validator::make( $fields, [
      'key' => 'required',
    ] );

    if ( $validator->fails() ) {
      return [
        'success' => false,
        'data'    => $data
      ];
    }
    $settings->update( $fields );
//    $translates->update( $fields );

    return new SettingsResource( $settings );

  }

  public function destroy( Settings $settings ) {
    if ( $settings->hasMedia() ) {
      $settings->getFirstMedia( 'images' )->delete();
    }
    $settings->delete();
    return [
      'success' => true,
    ];
  }

  public function uploadTo( Settings $settings, Request $request ) {
    return [
      'success' => true,
      'data'    => self::uploadToSettings( $settings, $request ),
    ];
  }


}
