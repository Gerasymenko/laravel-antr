<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PageCollection;
use App\Http\Resources\PageListResource;
use App\Http\Resources\PageResource;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use Transliterate;

class PagesController extends ApiController {
  public function index() {
    return PageListResource::collection( Page::paginate( 50 ) );
  }

  public function show( Page $page, $locale = false ) {
    \App::setLocale($locale);

    return new PageResource( $page );
  }

  public function destroy( Page $page ) {
    $page->delete();

//    return response( null, 204 );
    return [
      'success' => true,
    ];
  }

  public function update( Page $page, Request $request ) {
    $fields = $this->getPostData($request);

    if ( !$fields ) {
      return [
        'success' => false,
      ];
    }

    $page->update( $fields );

    if (!isset($fields['thumbnail']) || !$fields['thumbnail']) {
      $page->clearMediaCollection('thumbnail');
//      $thumb = is_array($data['thumbnail']) ? $data['thumbnail'][0] : $data['thumbnail'];
//      $full_path = storage_path( "app/{$thumb}" );
//      $explode = explode('/', $thumb);
//
//      if ( file_exists($full_path) && count($explode) < 3 ) {
//        $page->addMedia($full_path)->toMediaCollection('thumbnail');
//      }
    }

    return new PageResource( $page );
  }

  public function store( Request $request ) {
    $fields = $this->getPostData($request);

    if ( !$fields ) {
      return [
        'success' => false,
      ];
    }

    $page = Page::create( $fields );
    if (isset($fields['thumbnail']) && !empty($fields['thumbnail'])) {
      $thumb = is_array($fields['thumbnail']) ? $fields['thumbnail'][0] : $fields['thumbnail'];

      $page->addMedia($thumb)->toMediaCollection('thumbnail');
    }

    return [
      'success' => true,
      'item' => new PageResource( $page ),
    ];
  }

  public function getPostData( Request $request ) {

    $data = $request->all();

    if (isset($data['locale'])) {
      \App::setLocale($data['locale']);
    }

    $fields = ( isset($data['item']) ) ? $data['item'] : $data;

    $validator = Validator::make($fields, [
      'title' => 'required',
      'status' => 'required',
    ] );

    if ($validator->fails()) {
      return false;
    } else {
      return $fields;
    }
  }

  public function uploadTo(Page $page, Request $request) {
    return self::uploadToPost($page, $request);
  }
}
