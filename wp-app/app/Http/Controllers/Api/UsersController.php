<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UsersResource;
use Illuminate\Http\Request;
use App\User;

class UsersController extends ApiController {
  public function index() {
    return UsersResource::collection( User::paginate( 50 ) );
  }

  public function show( User $user ) {
    return new UsersResource( $user );
  }

  public function store( Request $request ) {
    $data = $request->all();

    $fields = (isset( $data['item'] )) ? $data['item'] : $data;

    if ( !$fields ) {
      return [
        'success' => false,
      ];
    }
    $fields['password'] = \Hash::make( $fields['password'] );

    $user = User::create( $fields );

    return [
      'success' => true,
      'item'    => new UsersResource( $user ),
    ];
  }

  public function update( User $user, Request $request ) {
    $fields = $request->all();
    if ( empty( $fields['password'] ) ) {
      unset( $fields['password'] );
    } else  {
      $fields['password'] = \Hash::make( $fields['password'] );
    }
    $user->update( $fields );
    return new UsersResource( $user );
  }

  public function destroy( User $user ) {
    $user->delete();
    return [
      'success' => true,
    ];
  }


}
