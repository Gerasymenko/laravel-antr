<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Settings;
use Redirect, Response, DB, Config;
use Illuminate\Http\Request;
use Mail;


class EmailController extends Controller {
  
  static $to = '';
  static $data = [];

  public static function sendEmail( Request $request ) {
    $settings = Settings::all();
    $admin_email = Helper::getValueByKey( $settings, 'email' );

    self::$to = $admin_email; //admin email

    $request_all = $request->all();

    self::$data = [
      "title" => 'Contact Page callback',
      "name"  => $request_all['name'],
      "email" => $request_all['email']
    ];

    Mail::send( 'emails.contact', self::$data, function ( $message ) {
      $message->to( self::$to, self::$data['name'] )
        ->subject( self::$data['title'] );
    } );
  }
}
