<?php

namespace App\Http\Controllers;

use App\Http\Resources\WorkResource;
use Illuminate\Http\Request;
use App\Work;

class WorksController extends Controller {

  public function __construct() {
    $this->middleware( 'global' );
  }

  public function work($name) {
    $result = self::setPageData($name);

    $data = self::getData($name);
    $html = self::ssr($data);

    if ( $result ) {
      return view( 'home', ['html' => $html] );
    } else {
      return redirect('/');
    }
  }


  public static function getData($name = false) {
    $work = Work::where(['slug' => $name])->first();
    if ($work) {
      $resources = new WorkResource($work);

      $data = [
        'item' => $resources->toArray(),
        'next' => self::nextPost($work->id),
      ];

      return $data;
    } else {
      return [];
    }
  }

  public static function nextPost($id = false) {
    // get previous user id
    $first = Work::where( 'status', 'publish')->orderBy( 'id', 'asc' )->first();

    // get next user id
    $next = Work::where( [['id', '>', $id], ['status', 'publish']]  )->orderBy( 'id', 'asc' )->first();

    if ( $next || $first ) {
      if ( $next ) {
        $resources = new WorkResource( $next );
      } else {
        $resources = new WorkResource( $first );
      }

      $data = $resources->toArray();

      return $data;

    } else {
      return [];
    }
  }

//  public function AjaxNextPost(Request $request) {
//    $all = $request->all();
//    \App::setLocale($all->lang);
//
//    $data = self::nextPost($all->id);
//
//    return response()->json( ['success' => true, 'data' => $data] );
//  }


  public function AjaxPageData($name, $locale) {
//    $fields = $request->all();
    \App::setLocale($locale);
    $data = self::getData($name);

    return response()->json( ['success' => true, 'data' => $data] );
  }

  public static function setPageData($name = false) {
    $name = $name ?: self::getSelfName(2);

    $work = Work::where(['slug' => $name])->first();
    if ( $work ) {
      if ( isset( $work->seo_title ) ) {
        \Config::set( 'app.name', $work->seo_title );
      }
      if ( isset( $work->seo_description ) ) {
        \Config::set( 'app.description', $work->seo_description );
      }


      self::addJsVar( self::getData( $name ) );

      return true;
    } else {
      return false;
    }
  }

  public static function getSelfName($step = 1) {
    $trace = debug_backtrace();

    return $trace[$step]["function"];
  }
}
