<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingsResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray( $request ) {
    return [
      'id'    => $this->id,
      'type'  => $this->type,
      'group' => $this->group,
      'key'   => $this->key,
      'value' => $this->value,
      'title' => $this->title,
      'order' => $this->order,
      'date'  => $this->updated_at,
    ];
  }
}
