<?php

namespace App\Http\Resources;

use App\Work;
use Illuminate\Http\Resources\Json\JsonResource;

class WorkResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray( $request = false ) {
//        return parent::toArray($request);
    /** @var $this Work */
    return [
      'id'              => $this->id,
      'title'           => $this->title,
      'slug'            => $this->slug,
      'description'     => $this->description ? $this->description : '',
      'content'         => $this->content ? $this->content : '',
      'status'          => $this->status,
      'date'            => $this->updated_at,
      'thumbnail'       => $this->getFirstMediaUrl( 'thumbnail' ),
      'seo_title'       => $this->seo_title ? $this->seo_title : '',
      'seo_description' => $this->seo_description ? $this->seo_description : '',
      'type'            => 'works',
    ];
  }

}
