<?php

namespace App\Http\Resources;

use App\Page;
use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request = false)
    {
//        return parent::toArray($request);
      /** @var $this Page */
//      var_dump($this->template);
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'description' => $this->description ? $this->description : '',
            'content' => $this->content ? $this->content : '',
//            'date' => date_format($this->updated_at, 'd.m.Y h:s'),
            'status' => $this->status,
            'date' => $this->updated_at,
            'template' => $this->template,
            'thumbnail' => $this->getFirstMediaUrl('thumbnail'),
            'seo_title' => $this->seo_title ? $this->seo_title : '',
            'seo_description' => $this->seo_description ? $this->seo_description : '',
            'type' => 'pages',
//            'thumbnail' => $this->getMedia('thumbnail'),
        ];
    }
}
