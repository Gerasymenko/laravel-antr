<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PageListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'title' => $this->title,
        'slug' => $this->slug,
        'description' => $this->description ? Str::words($this->description, 8, ' ...') : '',
//        'content' => $this->content ? $this->content : '',
        'status' => $this->status,
        'date' => $this->updated_at,
//        'thumbnail' => $this->getFirstMediaUrl('thumbnail'),
      ];
    }
}
