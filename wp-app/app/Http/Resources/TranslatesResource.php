<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TranslatesResource extends JsonResource {
  /**
   * Transform the resource into an array.
   *
   * @param  \Illuminate\Http\Request $request
   * @return array
   */
  public function toArray( $request ) {
    return [
      'id'     => $this->id,
      'locale' => $this->locale,
      'key'    => $this->key,
      'value'  => $this->value,
      'date'   => $this->updated_at,
    ];
  }
}
