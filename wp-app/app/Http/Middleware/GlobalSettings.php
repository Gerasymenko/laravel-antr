<?php
  
  namespace App\Http\Middleware;
  
  use App\Helpers\Helper;
  use App\Page;
  use App\Settings;
  use App\Translates;
  use App\Menu;
  use App\Work;
  use Closure;
  use function GuzzleHttp\Promise\all;
  
  class GlobalSettings {
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle( $request, Closure $next ) {
      self::setGlobalAssets();
      
      return $next( $request );
    }
    
    public static function setGlobalAssets() {
      self::addAsset( 'vue-front.js' );
      self::addAsset( 'vue-front.css' );
      
      $settings = self::getGlobalSettings();
      
      self::addJsVar( $settings, 'global' );
    }
    
    public static function getGlobalSettings() {
      $route = \Route::getCurrentRoute();
      if ( $route ) {
        $name    = explode( '.', $route->getName() );
        $current = array_pop( $name );
      } else {
        $current = false;
      }
      
      return [
        'settings'    => Settings::all(),
        'translates'  => Translates::all(),
        'menu'        => self::getMenu(),
        'locales'     => [
          'all'     => \Locales::all(),
          'current' => \Locales::current(),
          'default' => config( 'app.fallback_locale' ),
        ],
        'currentPage' => $current,
        'apiUrls'     => [
          'pages' => url( "/api/pages/" ),
        ],
      ];
    }
    
    public static function getMenu() {
      $menu   = Menu::all();
      $result = [];
      
      foreach ( $menu as $item ) {
        $data = self::getItemByIdType( $item->page_id );
        $data->order = $item->order;
        $result[] = $data;
      }
      
      return $result;
    }
    
    public static function getItemByIdType( $id, $type = 'page' ) {
      $result = [];
      switch ($type) {
        case 'page':
          $result = [];
          $pages  = Page::all();
          
          foreach ( $pages as $item ) {
            if ( $item->id == $id ) {
              $result = $item;
            }
          }
          break;
      }
      
      return $result;
    }
    
    
    public static function addAsset( $file ) {
      \Asset::add( $file );
    }
    
    public static function addJsVar( $vars, $group = false ) {
      if ( $group ) {
        \Asset::add( $vars, 'var', $group );
      } else {
        \Asset::add( $vars, 'var' );
      }
    }
  }
