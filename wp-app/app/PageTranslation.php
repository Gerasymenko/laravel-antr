<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model {
  protected $fillable = [ 'locale', 'title', 'content', 'description', 'seo_title', 'seo_description' ];

  public $timestamps = false;

  public function page() {
    return $this->belongsTo( 'App\Page' );
  }

  public function getSeoTitleAttribute($value) {
    return ( isset($value) && !empty($value) ) ? $value : $this->title;
  }

}
