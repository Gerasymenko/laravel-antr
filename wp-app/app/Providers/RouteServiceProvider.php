<?php

namespace App\Providers;

use App\Http\Resources\PageListResource;
use App\Http\Resources\SettingsResource;
use App\Http\Resources\WorkListResource;
use App\Page;
use App\Settings;
use App\Work;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider {
  /**
   * This namespace is applied to your controller routes.
   *
   * In addition, it is set as the URL generator's root namespace.
   *
   * @var string
   */
  protected $namespace = 'App\Http\Controllers';

  /**
   * Define your route model bindings, pattern filters, etc.
   *
   * @return void
   */
  public function boot() {
    parent::boot();

//      $this->mapPagesRoutes();
  }

  /**
   * Define the routes for the application.
   *
   * @return void
   */
  public function map() {
    $this->mapApiRoutes();

    $this->mapWebRoutes();

    $this->mapPagesRoutes();

    $this->mapWorksRoutes();
  }

  /**
   * Define the "web" routes for the application.
   *
   * These routes all receive session state, CSRF protection, etc.
   *
   * @return void
   */
  protected function mapWebRoutes() {
    Route::middleware( 'web' )
      ->namespace( $this->namespace )
      ->group( base_path( 'routes/web.php' ) );
  }

  /**
   * Define the "api" routes for the application.
   *
   * These routes are typically stateless.
   *
   * @return void
   */
  protected function mapApiRoutes() {
    Route::prefix( 'api' )
      ->middleware( 'api' )
      ->namespace( $this->namespace )
      ->group( base_path( 'routes/api.php' ) );
  }

  protected function mapPagesRoutes() {
    Route::middleware( 'web' )
      ->namespace( $this->namespace )->group( function () {
        $front_page = Settings::where( [ 'key' => 'front_page' ] )->first()->value;
        $pages = PageListResource::collection( Page::where( [ 'status' => 'publish' ] )->get() );

        foreach ( $pages as $page ) {
          $slug = $page->slug;

          if ( $slug !== $front_page ) {
//            $namespace = $this->namespace . '\PagesController';
            Route::multilingual( "/{$slug}/", "PagesController@{$slug}" )->name( $slug );
          }
        }
      } );
  }

  protected function mapWorksRoutes() {
    Route::middleware( 'web' )
      ->namespace( $this->namespace )->group( function () {
        $pages = WorkListResource::collection( Work::where( [ 'status' => 'publish' ] )->get() );

        foreach ( $pages as $page ) {
//          $slug = $page->slug
          Route::multilingual( "/work/{slug}/", "WorksController@work" )->name( $page->slug );
        }
      } );
  }
}
