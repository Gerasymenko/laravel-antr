<?php

namespace App\Providers;

//use Fisharebest\LaravelAssets\Assets;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        \Asset::add('app.js');
        \Asset::add('app.css');


        view()->composer( 'layouts.app-clear', function ( $view ) {
            $myvar = 'test';
//            $view->with('data', array('myvar' => $myvar));
            $view->with( 'myvar', $myvar );
        } );
    }
}
