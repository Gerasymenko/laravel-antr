<?php


namespace App\Providers\Classes;

use InvalidArgumentException;
use League\Flysystem\Util;

class Asset {

  /**
   * Regular expression to match a CSS url
   */
  const REGEX_CSS = '/\.css$/i';

  /**
   * Regular expression to match a JS url
   */
  const REGEX_JS = '/\.js$/i';

  /**
   * Regular expression to match a minified CSS url
   */
  const REGEX_MINIFIED_CSS = '/[.-]min\.css$/i';

  /**
   * Regular expression to match a minified JS url
   */
  const REGEX_MINIFIED_JS = '/[.-]min\.js$/i';

  /**
   * Regular expression to match an external url
   */
  const REGEX_EXTERNAL_URL = '/^((https?:)?\/\/|data:)/i';

  /**
   * File type detection options
   */
  const TYPE_CSS = 'css';
  const TYPE_JS = 'js';
  const TYPE_JS_VAR = 'var';
  const TYPE_AUTO = 'auto';

  /**
   * File group options.  Most sites will only use the default group.
   */
  const GROUP_DEFAULT = '';
  const DEFAULT_VARS = 'viewData';

  /**
   * Format HTML links using printf()
   */
  const FORMAT_CSS_LINK = '<link%s rel="stylesheet" href="%s">';
  const FORMAT_JS_LINK = '<script%s src="%s"></script>';

  /**
   * Format inline assets using printf()
   */
  const FORMAT_CSS_INLINE = '<style>%s</style>';
  const FORMAT_JS_INLINE = '<script>%s</script>';

  /**
   * Enable the pipeline and minify functions.
   *
   * @var bool
   */
  private $enabled;

  /**
   * Where do we read CSS files.
   *
   * @var string
   */
  private $css_source;

  /**
   * Where do we read JS files.
   *
   * @var string
   */
  private $js_source;

  /**
   * Where do we write CSS/JS files.
   *
   * @var string
   */
  private $destination;

  /**
   * Where does the client read CSS/JS files.
   *
   * @var string
   */
  private $destination_url;

  /**
   * Assets smaller than this will be rendered inline, saving an HTTP request.
   *
   * @var int
   */
  private $inline_threshold;

  /**
   * Create compressed version of assets, to support the NGINX gzip_static option.
   *
   * @var int
   */
  private $gzip_static;

  /**
   * Predefined sets of resources.  Can be nested to arbitrary depth.
   *
   * @var string[]|array[]
   */
  private $collections;

  /**
   * CSS assets to be processed
   *
   * @var string[][]
   */
  private $css_assets = array();

  /**
   * Javascript assets to be processed
   *
   * @var string[][]
   */
  private $js_assets = array();

  /**
   * Javascript assets to be processed
   *
   * @var string[][]
   */
  private $js_vars = array();

//    /**
//     * Create an asset manager.
//     *
//     * @param array      $config     The local config, merged with the default config
//     */
  public function __construct( array $config ) {
    $this
      ->setEnabled( $config['enabled'] )
      ->setCssSource( $config['css_source'] )
      ->setJsSource( $config['js_source'] )
      ->setDestination( $config['destination'] )
      ->setDestinationUrl( $config['destination_url'] )
      ->setInlineThreshold( $config['inline_threshold'] )
      ->setGzipStatic( $config['gzip_static'] )
      ->setCollections( $config['collections'] );
  }

  /**
   * @param string $css_source
   *
   * @return Asset
   */
  public function setCssSource( $css_source ) {
    $this->css_source = trim( $css_source, '/' );

    return $this;
  }

  /**
   * @return string
   */
  public function getCssSource() {
    return $this->css_source;
  }

  /**
   * @param string $js_source
   *
   * @return Asset
   */
  public function setJsSource( $js_source ) {
    $this->js_source = trim( $js_source, '/' );

    return $this;
  }

  /**
   * @return string
   */
  public function getJsSource() {
    return $this->js_source;
  }

  /**
   * @param string $destination
   *
   * @return Asset
   */
  public function setDestination( $destination ) {
    $this->destination = trim( $destination, '/' );

    return $this;
  }

  /**
   * @return string
   */
  public function getDestination() {
    return $this->destination;
  }

  /**
   * An (optional) absolute URL for fetching generated assets.
   *
   * @param string $destination_url
   *
   * @return Asset
   */
  public function setDestinationUrl( $destination_url ) {
    $this->destination_url = rtrim( $destination_url, '/' );

    return $this;
  }

  /**
   * @return string
   */
  public function getDestinationUrl() {
    return $this->destination_url;
  }

  /**
   * @param boolean $enabled
   *
   * @return Asset
   */
  public function setEnabled( $enabled ) {
    $this->enabled = (bool)$enabled;

    return $this;
  }

  /**
   * @return boolean
   */
  public function isEnabled() {
    return $this->enabled;
  }

  /**
   * @param int $inline_threshold
   *
   * @return Asset
   */
  public function setInlineThreshold( $inline_threshold ) {
    $this->inline_threshold = (int)$inline_threshold;

    return $this;
  }

  /**
   * @return int
   */
  public function getInlineThreshold() {
    return $this->inline_threshold;
  }

  /**
   * @param int $gzip_static
   *
   * @return Asset
   */
  public function setGzipStatic( $gzip_static ) {
    $this->gzip_static = (int)$gzip_static;

    return $this;
  }

  /**
   * @return int
   */
  public function getGzipStatic() {
    return $this->gzip_static;
  }

  /**
   * @param array[]|string[] $collections
   *
   * @return Asset
   */
  public function setCollections( $collections ) {
    $this->collections = $collections;

    return $this;
  }

  /**
   * @return array[]|string[]
   */
  public function getCollections() {
    return $this->collections;
  }

  /**
   * Add one or more assets.
   *
   * @param string|string[] $asset A local filename, a remote URL or the name of a collection.
   * @param string $type Force a file type, "css" or "js", instead of using the extension.
   * @param string $group Optionally split your assets into multiple groups, such as "head" and "body".
   *
   * @return Asset
   */
  public function add( $asset, $type = self::TYPE_AUTO, $group = self::GROUP_DEFAULT ) {
    if ( $type === self::TYPE_JS_VAR && empty( $group ) ) {
      $group = self::DEFAULT_VARS;
    }

    $this->checkGroupExists( $group );


    if ( is_array( $asset ) ) {
      foreach ( $asset as $k => $a ) {
        if ( $type === self::TYPE_JS_VAR ) {
          $this->js_vars[ $group ][ $k ] = $a;
        } else {
          $this->add( $a, $type, $group );
        }
      }
    } elseif ( $type === self::TYPE_CSS || $type === self::TYPE_AUTO && preg_match( self::REGEX_CSS, $asset ) ) {
      if ( !in_array( $asset, $this->css_assets[ $group ] ) ) {
        $this->css_assets[ $group ][] = $asset;
      }
    } elseif ( $type === self::TYPE_JS || $type === self::TYPE_AUTO && preg_match( self::REGEX_JS, $asset ) ) {
      if ( !in_array( $asset, $this->js_assets[ $group ] ) ) {
        $this->js_assets[ $group ][] = $asset;
      }
    } elseif ( gettype($asset) === 'string' && array_key_exists( $asset, $this->collections ) ) {
      $this->add( $this->collections[ $asset ], $type, $group );
    } else {
      throw new InvalidArgumentException( 'Unknown asset type: ' . $asset );
    }

    return $this;
  }

  /**
   * Render markup to load the CSS assets.
   *
   * @param string $group Optionally split your assets into multiple groups, such as "head" and "body".
   * @param string[] $attributes Optional attributes, such as ['media' => 'print']
   *
   * @return string
   */
  public function css( $group = self::GROUP_DEFAULT, array $attributes = [] ) {
    $this->checkGroupExists( $group );

    return $this->processAssets(
      $attributes,
      $this->css_assets[ $group ],
      '.css',
      $this->getCssSource(),
      self::FORMAT_CSS_LINK,
      self::FORMAT_CSS_INLINE
    );
  }

  /**
   * Render markup to load the JS assets.
   *
   * @param string $group Optionally split your assets into multiple groups, such as "head" and "body".
   * @param string[] $attributes Optional attributes, such as ['async']
   *
   * @return string
   */
  public function js( $group = self::GROUP_DEFAULT, array $attributes = [] ) {
    $this->checkGroupExists( $group );

    return $this->processAssets(
      $attributes,
      $this->js_assets[ $group ],
      '.js',
      $this->getJsSource(),
      self::FORMAT_JS_LINK,
      self::FORMAT_JS_INLINE
    );
  }

  public function jsVars() {
//        $html_attributes = $this->convertAttributesToHtml($attributes);
    $html_links = '';

    foreach ( $this->js_vars as $group => $vars ) {
      $content = "window.{$group} = {};";
      $html_links .= sprintf( self::FORMAT_JS_INLINE, $content );

      foreach ( $vars as $key => $value ) {
        if ( !is_string( $value ) ) {
          $value = json_encode( $value, JSON_UNESCAPED_UNICODE );
        } elseif ( is_string( $value ) ) {
          $value = "'$value'";
        }

        $content = "window.{$group}.{$key} = {$value};";
        $html_links .= sprintf( self::FORMAT_JS_INLINE, $content );
      }
    }

    return $html_links;
  }


  /**
   * Make sure that the specified group (i.e. array key) exists.
   *
   * @param string $group
   */
  private function checkGroupExists( $group ) {
    if ( !array_key_exists( $group, $this->css_assets ) ) {
      $this->css_assets[ $group ] = [];
    }
    if ( !array_key_exists( $group, $this->js_assets ) ) {
      $this->js_assets[ $group ] = [];
    }
    if ( !array_key_exists( $group, $this->js_vars ) ) {
      $group = empty( $group ) ? self::DEFAULT_VARS : $group;

      if ( !array_key_exists( $group, $this->js_vars ) ) {
        $this->js_vars[ $group ] = [];
      }
    }
  }

  /**
   * Render markup to load the CSS or JS assets.
   *
   * @param string[] $attributes Optional attributes, such as ['async']
   * @param string[] $assets The files to be processed
   * @param string $extension ".css" or ".js"
   * @param string $source_dir The folder containing the source assets
   * @param string $format_link Template for an HTML link to the asset
   * @param string $format_inline Template for an inline asset
   *
   * @return string
   */
  private function processAssets(
    array $attributes,
    array $assets,
    $extension,
    $source_dir,
    $format_link,
    $format_inline
  ) {
    $urls = [];
    $path = $this->getDestination();

    $url = url( $source_dir );

    return $this->htmlLinks( $url, $assets, '', $format_link, $attributes );
  }

  /**
   * Generate HTML links to a list of processed asset files.
   *
   * @param string $url path to the assets
   * @param string[] $hashes base filename
   * @param string $extension ".css", ".min.js", etc.
   * @param string $format
   * @param string[] $attributes
   *
   * @return string
   */
  private function htmlLinks( $url, $hashes, $extension, $format, $attributes ) {
    $html_attributes = $this->convertAttributesToHtml( $attributes );

    $html_links = '';
    foreach ( $hashes as $asset ) {
      $html_links .= sprintf( $format, $html_attributes, $url . '/' . $asset . $extension );
    }

    return $html_links;
  }

  /**
   * Convert an array of attributes to HTML.
   *
   * @param string[] $attributes
   *
   * @return string
   */
  private function convertAttributesToHtml( array $attributes ) {
    $html = '';
    foreach ( $attributes as $key => $value ) {
      if ( is_int( $key ) ) {
        $html .= ' ' . $value;
      } else {
        $html .= ' ' . $key . '="' . $value . '"';
      }
    }

    return $html;
  }

  /**
   * Is a URL absolute or relative?
   *
   * @param string $url
   *
   * @return bool
   */
  public function isAbsoluteUrl( $url ) {
    return preg_match( self::REGEX_EXTERNAL_URL, $url ) === 1;
  }

  /**
   * Normalize a path, removing '.' and '..' folders. e.g.
   *
   * "a/b/./c/../../d" becomes "a/d"
   *
   * @param string $url
   *
   * @return string
   */
  public function normalizePath( $url ) {
    while ( strpos( $url, '/./' ) !== false ) {
      $url = str_replace( '/./', '/', $url );
    }
    while ( strpos( $url, '/../' ) !== false ) {
      $url = preg_replace( '/[^\/]+\/\.\.\//', '', $url, 1 );
    }

    return $url;
  }

  /**
   * Create a relative path between two URLs.
   *
   * e.g. the relative path from "a/b/c" to "a/d" is "../../d"
   *
   * @param string $source
   * @param string $destination
   *
   * @return string
   */
  public function relativePath( $source, $destination ) {
    if ( $source === '' ) {
      return $destination;
    }

    $parts1 = explode( '/', $source );
    $parts2 = explode( '/', $destination );

    while ( !empty( $parts1 ) && !empty( $parts2 ) && $parts1[0] === $parts2[0] ) {
      array_shift( $parts1 );
      array_shift( $parts2 );
    }

    return str_repeat( '../', count( $parts1 ) ) . implode( '/', $parts2 );
  }

}
