<?php

namespace App\Providers;

use App\Providers\Classes\Locales;
use Illuminate\Support\ServiceProvider;

class TranslatableServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
  public function register()
  {
    $configPath = __DIR__.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'translatable.php';

    $this->mergeConfigFrom($configPath, 'transliterate');

    $this->registerTranslatableHelper();
  }

    /**
     * Bootstrap services.
     *
     * @return void
     */
  public function boot()
  {
    $configPath = __DIR__.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'translatable.php';

    $this->publishes([
      $configPath => config_path('translatable.php'),
    ], 'translatable');
  }

  public function registerTranslatableHelper()
  {
    $this->app->singleton('translatable.locales', Locales::class);
    $this->app->singleton(Locales::class);

    $this->app->singleton('locales', function ($app) {
      return app(Locales::class);
    });
  }

  public function provides()
  {
    return [
      'translatable.helper',
      Locales::class,
    ];
  }
}
