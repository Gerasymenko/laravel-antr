<?php


namespace App\Providers;


use App\Providers\Classes\Asset;
use Illuminate\Support\ServiceProvider;

class AssetServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     */
    public function register() {
        // Bind our component into the IoC container.
        $this->app->singleton('asset', function ($app) {
            return new Asset($app['config']['asset']);
        });
    }

}
