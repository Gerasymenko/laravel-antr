<?php


namespace App\Providers\Facade;


use Illuminate\Support\Facades\Facade;

class LocalesFacade extends Facade {
  /**
   * Get the registered name of the component.
   *
   * @return string
   */
  protected static function getFacadeAccessor()
  {
    return 'locales';
  }
}
