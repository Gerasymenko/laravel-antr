<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkTranslation extends Model
{
  protected $fillable = ['language', 'title', 'content', 'description', 'seo_title', 'seo_description'];

  public $timestamps = false;

  public function page()
  {
    return $this->belongsTo('App\Work');
  }
}
