<?php

namespace App;

use App\Providers\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;


class Translates extends Model {
  use TranslatableTrait;


  public $translatedAttributes = ['locale','value'];
  protected $fillable = [ 'key' ];

  /**
   * Change updated date format
   * @param $value
   * @return false|string
   */
  public function getUpdatedAtAttribute( $value ) {
    $format = 'd.m.Y H:i';
    return date( $format, strtotime( $value ) );
  }


  public function translation($language = null)
  {
    if ($language == null) {
      $language = App::getLocale();
    }
    return $this->hasMany('App\TranslatesTranslation')->where('locale', '=', $language);
  }

}
