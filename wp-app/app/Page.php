<?php

namespace App;

use App\Providers\Traits\TranslatableTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Support\Facades\App;
use Transliterate;

class Page extends Model implements HasMedia {

  use HasMediaTrait;
  use TranslatableTrait;

//  public $primaryKey = 'slug';

  public $translatedAttributes = ['title', 'content', 'description', 'seo_title', 'seo_description'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['slug', 'status', 'template'];

  /**
   * Change updated date format
   * @param $value
   * @return false|string
   */
  public function getUpdatedAtAttribute( $value ) {
    $format = 'd.m.Y H:i';

    return date( $format, strtotime( $value ) );
//        return $value;
  }

  public function getContentAttribute($value) {
    if ( $this->attributes['template'] ) {
      return json_decode($value);
    }

    return $value;
  }

  /**
   * Register collection
   */
  public function registerMediaCollections() {
    $this
      ->addMediaCollection( 'thumbnail' )
      ->singleFile();
  }

  public function setSlugAttribute($value) {
      if ( empty($value) || !isset($value) ) {
        $this->attributes['slug'] = Transliterate::slugify($this->title);
      } else {
        $this->attributes['slug'] = $value;
      }
  }

//  public function translation($language = null)
//  {
//    if ($language == null) {
//      $language = App::getLocale();
//    }
//    return $this->hasMany('App\PageTranslation')->where('locale', '=', $language);
//  }

}
