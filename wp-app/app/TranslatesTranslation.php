<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TranslatesTranslation extends Model
{
  protected $fillable = ['value'];

  public $timestamps = false;

  public function page()
  {
    return $this->belongsTo('App\Translates');
  }
}
