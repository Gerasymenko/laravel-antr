<?php
  /**
   * Created by PhpStorm.
   * User: gerasart
   * Date: 1/15/2020
   * Time: 5:12 PM
   */
  
  namespace App\Helpers;
  
  
  class Helper {
    
    public static function getValueByKey( $data, $key ) {
      $res = [];
      foreach ( $data as $item ) {
        if ( $item['key'] == $key ) {
          $res = $item;
          break;
        }
      }
      return $res['value'];
    }
    
  }
