<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Auth;

class AdminSettings {

  public static function globalUrls() {
    return [
    ];
  }

  public static function globalActions() {
    return [
      'login' => route( 'login/authenticate' ),
      'logout' => route( 'admin/logout' ),
      'registration' => route( 'registration' ),
      'restore' => route('restore'),
    ];
  }

  public static function userData() {
    return Auth::user();
  }

}
