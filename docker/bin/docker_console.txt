========================================================================================================================
Docker Compose
========================================================================================================================

docker-compose create
docker-compose up -d   Чтобы запустить файл
docker-compose down --volumes
docker-compose ps

docker-compose -f docker-compose-staging.yml up -d
docker-compose -f docker-compose-staging.yml ps
docker-compose -f docker-compose-staging.yml scale wp=2
127.0.1.1 wp.local

docker-compose logs -f

========================================================================================================================
Docker
========================================================================================================================

docker network ls
docker attach <container name>
docker network inspect bridge

docker pull                            # скачати образ
docker images                          # список всіх скачаних образів
docker ps                              # запущенные контейнеры
docker ps -a                           # увидеть помимо запущенных и потушенные контейнеры
docker ps -q                           # показати лише ідентифікатори контейнерів
docker build -t                        # создаст и запустит контейнер
docker run nginx                       # создаст и запустит контейнер 
docker inspect <container name>        # Просмотр уже подключённых папок
docker restart docker-nginx
docker restart <container name>
docker rename  <container name>
docker stop    <container name>
docker kill    <container name> 
docker kill $(docker ps -q)            # Del all   
docker rm -f   <container name>        # Удалить контеинер
docker rm $(docker ps -aq)             # видалити всі контейнери
docker rmi $(docker images -a -q)
docker rmi $(docker images -q)
docker rmi                             # видалити образ
docker system prune -a
docker exec -ti <container name> bash  # Войти в контеинер
docker run -t -i debian /bin/bash      # запустити інтерактивну програму в контейнері
docker run -d debian /bin/sh -c "while true; do echo hello world; sleep 1; done"  # запустити демона в контейнері
docker run -v /host/dir:/container/dir debian # запустити контейнер, що директорію хоста /host/dir буде змонтовано як /container/dir
docker logs -f ecstatic_lovelace       # слідкувати за логами демона в контейнері
docker logs nginx <container name>     # слідкувати за логами
docker build -t ouruser/ourrepo .      # збудувати образ з Dockerfile поточної директорії
docker network create name-proxy       # Создайте сеть Docker для поддержки взаимодействия прокси-сервера и контейнеров


docker run -d — name nginx -p 8080:80 nginx
-d   запускает контейнер в бэкграунде
-p   паблишит порт(форвардит порт из системы) в контейнер


docker run -d --name=db -e MYSQL_ROOT_PASSWORD=password mysql:5.7   //  Запускаем базу данных
docker run -d --name=wp --link=db -p 80:80 -v   Запускаем Вордпресс, коннектим его к бд и монтируем в контейнер нашу тему:

========================================================================================================================
MySQL
========================================================================================================================

mysql -uroot -proot 
CREATE DATABASE <name_db>;          # Ubuntu create db
CREATE USER FirstUser@localhost;
GRANT ALL PRIVILEGES ON <name_db>.* TO FirstUser@localhost IDENTIFIED BY 'FirstPassword';
SHOW DATABASES;


# Backup
docker exec CONTAINER /usr/bin/mysqldump -u root --password=root DATABASE > backup.sql

# Restore
cat backup.sql | docker exec -i CONTAINER /usr/bin/mysql -u root --password=root DATABASE

You could also create a function in your .bash_profile:

dumpdb () {
   docker exec "${PWD##*/}_my-db_1" \
       mysqldump -uroot --password=password "$1" >backup.sql
}

dumpdb "my_database"

========================================================================================================================
Ubuntu / Linux
========================================================================================================================

pwd                           # path to current folder
chmod 777 file                # change rights  
sudo chmod -R 755 /var/www
chmod  a-x  file              # change rights   
ls -la                        # file list
su -                          # root user
apt-get update				  # update
apt install openssh-server
apt-get install sudo
apt-get install git
apt-get install vim

========================================================================================================================
VIM
========================================================================================================================

VIM // https://eax.me/vim-commands/


$PWD

docker run -p 3306:3306 --name mysqlserver -e MYSQL_ROOT_PASSWORD=root -d mysql

docker run -tid -p 80:80 -v $PWD/www:/var/www/site --link mysqlserver:mysqldb apache-php


mysqldump -u my_db_user -p my_db_name > /path/to/save/backup_file.sql

https://github.com/roots/bedrock

bash wp-init.sh

https://github.com/willopez/NoomaPress

https://github.com/DevTM/wp-docker/blob/master/docker-compose.yml

https://www.youtube.com/watch?v=XyFJx9APCHk

https://github.com/phpmyadmin/docker/issues/23

https://stackoverflow.com/questions/38406013/prevent-skip-in-docker-compose

========================================================================================================================
Install Docker & Docker-Compose to Ubuntu
========================================================================================================================

sudo -i
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
curl -sSL https://get.docker.com/ | sh
curl -L https://github.com/docker/compose/releases/download/1.5.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
usermod -aG docker ubuntu
sudo reboot


========================================================================================================================
Nginx
========================================================================================================================

user  nginx;
worker_processes  1;
error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  worker_connections  1024;
}

http {
  ...
  # Пропущенны дефолтные настройки
  ...
  # Вот тут начинаются мои сайты:
  server {
    listen 80;
    server_name test.com;
    root /usr/share/nginx/html/test.com;
  }

  server {
    listen 80;
    server_name domain2.com;
    root /usr/share/nginx/html/domain2.com;
  }

  server {
    listen 80;
    server_name domain3.com;
    root /usr/share/nginx/html/domain3.com;
  }

  # А это пример настройки уже не статического сайта,
  # а динамического приложения, запущенного на порту 8080
  server {
    listen 80;
    server_name dinamicsite.com;
    location / {
      proxy_pass         http://<server_ip>:8080;
      proxy_set_header   X-Real-IP $remote_addr;
      proxy_set_header   Host      $http_host;
    }
  }

  include /etc/nginx/conf.d/*.conf;
}


========================================================================================================================
Nginx
========================================================================================================================


 $ docker run --name nginx-0 \
               -p 80:80 -d \
               -v /home/core/www:/usr/share/nginx/html:ro \
               -v /home/core/nginx.conf:/etc/nginx/nginx.conf:ro \
               --restart=always nginx
Разберём по пунктам что мы здесь наворотили:

--name nginx-0 задаём имя для контейнера, чтоб потом можно было удобно им управлять (останавливать, перезапускать).
-d говорим докеру, что контейнер должен быть демонизирован.
-p 80:80 nginx запущен на 80 порту внутри контейнера, пробрасываем этот порт наружу.
-v /home/core/www:/usr/share/nginx/html:ro монтируем папку с сайтами (~/www) к папке из которой nginx будет сервить контент.
-v /home/core/nginx.conf:/etc/nginx/nginx.conf:ro монтируем наш конфиг в контейнер.
--restart=always говорим докеру чтоб он перезапустил контейнер в случае если тот внезапно упадёт (мало ли что может случиться).
nginx имя имейджа в докер-хабе, он будет выкачан и запущен. Вот собственно и всё, ходим по сайтам и наслаждаемся результатом!