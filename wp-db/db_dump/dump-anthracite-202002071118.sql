-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 192.168.2.61    Database: anthracite
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.8-MariaDB-1:10.4.8+maria~bionic

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`manipulations`)),
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`custom_properties`)),
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`responsive_images`)),
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
INSERT INTO `media` VALUES (26,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',26,'2020-02-06 09:51:46','2020-02-06 09:51:46'),(27,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',27,'2020-02-06 09:55:01','2020-02-06 09:55:01'),(28,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',28,'2020-02-06 09:55:36','2020-02-06 09:55:36'),(29,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',29,'2020-02-06 09:57:19','2020-02-06 09:57:19'),(30,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',30,'2020-02-06 10:01:45','2020-02-06 10:01:45'),(31,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',31,'2020-02-06 10:02:41','2020-02-06 10:02:41'),(32,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',32,'2020-02-06 10:04:30','2020-02-06 10:04:30'),(33,'App\\Page',31,'image','about_image','about_image.png','image/png','media',1044941,'[]','[]','[]',33,'2020-02-06 10:09:25','2020-02-06 10:09:25'),(34,'App\\Settings',11,'images','logo','logo.svg','image/svg','media',110734,'[]','[]','[]',34,'2020-02-06 10:21:00','2020-02-06 10:21:00'),(35,'App\\Page',31,'leftImage','image_left','image_left.jpg','image/jpeg','media',140736,'[]','[]','[]',35,'2020-02-06 11:23:11','2020-02-06 11:23:11'),(36,'App\\Page',31,'rightImage','image_right','image_right.jpg','image/jpeg','media',193903,'[]','[]','[]',36,'2020-02-06 11:23:26','2020-02-06 11:23:26'),(37,'App\\Page',33,'image','person-1','person-1.png','image/png','media',164157,'[]','[]','[]',37,'2020-02-07 09:15:02','2020-02-07 09:15:02'),(38,'App\\Page',33,'image','person-1','person-1.png','image/png','media',164157,'[]','[]','[]',38,'2020-02-07 09:15:20','2020-02-07 09:15:20'),(39,'App\\Page',33,'image','person-2','person-2.png','image/png','media',56301,'[]','[]','[]',39,'2020-02-07 09:15:22','2020-02-07 09:15:22'),(40,'App\\Page',33,'image','person-3','person-3.png','image/png','media',39566,'[]','[]','[]',40,'2020-02-07 09:15:24','2020-02-07 09:15:24'),(41,'App\\Page',33,'image','about-header','about-header.png','image/png','media',481588,'[]','[]','[]',41,'2020-02-07 09:16:37','2020-02-07 09:16:37');
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2020_01_15_140217_create_pages_table',1),(4,'2020_01_15_142519_create_works_table',1),(5,'2020_01_17_091852_create_media_table',1),(6,'2020_01_23_162455_create_pages_translations_table',1),(7,'2020_01_24_125738_create_work_translations_table',1),(8,'2020_01_28_130902_create_translates_table',1),(9,'2020_01_28_140851_create_settings_table',1),(10,'2020_01_30_133220_create_translates_translations_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_translations`
--

DROP TABLE IF EXISTS `page_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_translations_page_id_foreign` (`page_id`),
  CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_translations`
--

LOCK TABLES `page_translations` WRITE;
/*!40000 ALTER TABLE `page_translations` DISABLE KEYS */;
INSERT INTO `page_translations` VALUES (31,'en','Sed ut perspiciatis unde omnis iste natus error',NULL,NULL,NULL,'{\"hero\":{\"leftImage\":\"/media/35/image_left.jpg\",\"rightImage\":\"/media/36/image_right.jpg\"},\"about\":{\"image\":\"/media/33/about_image.png\",\"title\":\"Way of Thinking\",\"description\":\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugitt.\"},\"projects\":{\"title\":\"Les derniers projets\"}}',31,NULL,NULL),(33,'en','About us',NULL,NULL,NULL,'{\"hero\":{\"image\":\"/media/41/about-header.png\",\"title\":\"Profound know-how and eclecticism.\",\"column1\":\"Pelizzari Studio was launched in 1991, within an heritage palace in Brescia’s historical centre. Its aim? To amaze and astonish. \\n\\nEach customer has specific requests and needs, therefore each project - be it home, hotel, retail - is taylor made, starting from architectural details to the choice of materials, all the way to the littlest and slightest detail. It’s on these premises that the studio evolved, realizing projects from London to Dubai, from Côte d’Azur to Lugano and Italy’s most renowned cities of arts.\",\"column2\":\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugitt.\"},\"teams\":{\"repeater\":[{\"image\":\"/media/40/person-3.png\",\"title\":\"Claudia Pelizzari\",\"description\":\"Described by Archilovers as “one of the feminine talents of contemporary architecture”, Claudia is an instinctive and radical interior designer with a consolidated international background.\",\"id\":0},{\"image\":\"/media/39/person-2.png\",\"title\":\"David Morini\",\"description\":\"Polyhedric Architect, sophisticated art connoisseur and spontaneous creative, David directs the Studio acting as its true incarnation.\",\"id\":1},{\"image\":\"/media/38/person-1.png\",\"title\":\"David Pelizzari\",\"description\":\"Described by Archilovers as “one of the feminine talents of contemporary architecture”, Claudia is an instinctive and radical interior designer with a consolidated international background.\",\"id\":2}]}}',33,NULL,NULL);
/*!40000 ALTER TABLE `page_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (31,'publish','home','home','2020-02-05 11:14:32','2020-02-05 14:08:21'),(33,'publish','about-us','about','2020-02-06 12:30:18','2020-02-06 12:40:45');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (6,'TypeTextarea','description','description','About Site 2','Description',0,'2020-02-04 16:17:06','2020-02-05 10:19:25'),(8,'TypeInput','copyrighter','copyrighter','Svitsoft','Copyrighter',2,'2020-02-04 16:49:29','2020-02-05 09:02:58'),(9,'TypeInput','email','email','test@gmail.com','Email',3,'2020-02-04 16:49:53','2020-02-06 13:55:39'),(10,'TypeInput','phone','phone','0934804438','Phone',2,'2020-02-04 16:50:17','2020-02-06 14:52:40'),(11,'TypeImage','logo','logo','/media/34/logo.svg','Logo',-1,'2020-02-06 10:20:27','2020-02-06 10:22:11'),(12,'TypeInput','socials','facebook','https://www.facebook.com/','Facebook',10,'2020-02-06 13:42:14','2020-02-06 16:09:17'),(14,'TypeInput','socials','instagram','https://www.instagram.com/','Instagram',8,'2020-02-06 13:53:13','2020-02-06 16:09:28'),(16,'TypeInput','socials','linkedin','https://www.linkedin.com/','LinkedIn',9,'2020-02-06 13:55:09','2020-02-06 13:59:26');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translates`
--

DROP TABLE IF EXISTS `translates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translates`
--

LOCK TABLES `translates` WRITE;
/*!40000 ALTER TABLE `translates` DISABLE KEYS */;
INSERT INTO `translates` VALUES (3,'slogan','2020-02-06 10:11:06','2020-02-06 10:11:06');
/*!40000 ALTER TABLE `translates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translates_translations`
--

DROP TABLE IF EXISTS `translates_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translates_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `translates_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `translates_translations_translates_id_foreign` (`translates_id`),
  CONSTRAINT `translates_translations_translates_id_foreign` FOREIGN KEY (`translates_id`) REFERENCES `translates` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translates_translations`
--

LOCK TABLES `translates_translations` WRITE;
/*!40000 ALTER TABLE `translates_translations` DISABLE KEYS */;
INSERT INTO `translates_translations` VALUES (7,'en','Don\'t be a stranger',3,NULL,NULL),(8,'ru','Don\'t be a stranger',3,NULL,NULL),(9,'ua','Don\'t be a stranger',3,NULL,NULL);
/*!40000 ALTER TABLE `translates_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Maksim','gerasymenkoph@gmail.com',NULL,'$2y$10$DfyRx7cnW2FwPpoVP8naT.afq1M3t/HE916MsaMhiG7wSM7GTTCAi',1,NULL,'2020-02-04 14:56:49','2020-02-04 14:56:49'),(2,'Chameleon2die4','chameleon2die4@gmail.com',NULL,'$2y$10$Bhji3RcNVYWw9JwUUcAuWuj7vj/xz7lpAdqlry9plO2/uKv/UYhUm',1,NULL,'2020-02-05 10:32:31','2020-02-05 10:32:31');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_translations`
--

DROP TABLE IF EXISTS `work_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `work_translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `work_translations_work_id_foreign` (`work_id`),
  CONSTRAINT `work_translations_work_id_foreign` FOREIGN KEY (`work_id`) REFERENCES `works` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_translations`
--

LOCK TABLES `work_translations` WRITE;
/*!40000 ALTER TABLE `work_translations` DISABLE KEYS */;
INSERT INTO `work_translations` VALUES (1,'en','Expedita quisquam quod perspiciatis.','Expedita quisquam quod perspiciatis.','Hic velit nesciunt voluptatem sed accusamus nobis. Beatae qui voluptas voluptatibus molestiae est sit laudantium.','Hic velit nesciunt voluptatem sed accusamus nobis. Beatae qui voluptas voluptatibus molestiae est sit laudantium.','<html><head><title>Et aut est.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">qui</label><input type=\"text\" id=\"username\"><label for=\"password\">quia</label><input type=\"password\" id=\"password\"></form><div class=\"et\"></div></body></html>\n',1,NULL,NULL),(2,'en','Id itaque optio optio consequatur est exercitationem.','Id itaque optio optio consequatur est exercitationem.','Hic quas non ut. Nisi voluptates quo quis sapiente quod. Veritatis hic quae omnis maiores necessitatibus enim corrupti.','Hic quas non ut. Nisi voluptates quo quis sapiente quod. Veritatis hic quae omnis maiores necessitatibus enim corrupti.','<html><head><title>Praesentium fuga.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">ut</label><input type=\"text\" id=\"username\"><label for=\"password\">voluptatum</label><input type=\"password\" id=\"password\"></form><div class=\"qui\"></div><div id=\"21322\"><div class=\"numquam\"></div><div id=\"79692\"></div><div class=\"sit\"></div><div class=\"dolorem\"></div></div><div id=\"95498\"></div></body></html>\n',2,NULL,NULL),(3,'en','Quia in ex debitis perferendis.','Quia in ex debitis perferendis.','Similique sapiente consequatur incidunt quidem quo accusantium. Provident est quae temporibus voluptate repudiandae quo. In qui facilis quis vel maiores doloremque.','Similique sapiente consequatur incidunt quidem quo accusantium. Provident est quae temporibus voluptate repudiandae quo. In qui facilis quis vel maiores doloremque.','<html><head><title>Est occaecati beatae quibusdam rem nulla numquam sequi.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">magnam</label><input type=\"text\" id=\"username\"><label for=\"password\">aliquam</label><input type=\"password\" id=\"password\"></form><div id=\"77717\"></div><div id=\"99069\"></div><div class=\"distinctio\"></div></body></html>\n',3,NULL,NULL),(4,'en','Magnam autem ad omnis culpa.','Magnam autem ad omnis culpa.','Repellat illo quaerat animi delectus dolores. Aut provident iusto cumque animi. Accusamus perspiciatis expedita possimus dolor sed omnis sed fugit.','Repellat illo quaerat animi delectus dolores. Aut provident iusto cumque animi. Accusamus perspiciatis expedita possimus dolor sed omnis sed fugit.','<html><head><title>Dolores ullam iusto quia numquam sunt tempore itaque.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">dolorum</label><input type=\"text\" id=\"username\"><label for=\"password\">aliquam</label><input type=\"password\" id=\"password\"></form><div id=\"70620\"><span>Autem doloribus.</span></div><div id=\"48438\"></div></body></html>\n',4,NULL,NULL),(5,'en','Amet eos fuga reprehenderit excepturi totam cumque ut.','Amet eos fuga reprehenderit excepturi totam cumque ut.','Saepe quis quaerat aliquam eligendi. Officiis dolorem repellendus id. Iusto dolores qui illum.','Saepe quis quaerat aliquam eligendi. Officiis dolorem repellendus id. Iusto dolores qui illum.','<html><head><title>Sunt illo natus pariatur quia sed.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">voluptate</label><input type=\"text\" id=\"username\"><label for=\"password\">corporis</label><input type=\"password\" id=\"password\"></form><div id=\"8762\"><ul><li>Qui autem voluptatibus.</li><li>Eligendi.</li><li>Eum nisi ipsam.</li><li>Veniam rem dignissimos.</li><li>Suscipit excepturi.</li><li>Officiis dolores harum numquam.</li><li>Inventore aliquam.</li><li>Deleniti illo.</li><li>Libero deleniti molestiae.</li><li>Repellat dolor.</li><li>Est aut quia rerum error.</li></ul><i>Quidem dicta suscipit laudantium ad architecto ipsum.</i><span>Sunt occaecati.</span></div><div id=\"39200\"><div class=\"repellendus\"></div></div><div class=\"hic\"><div class=\"quam\"></div><div id=\"38798\"><b>Ipsam maiores explicabo quos reiciendis.</b><i>Et sit.</i><b>Dolorem et eos earum minima nam dolore voluptates molestias nesciunt culpa et.</b></div><div class=\"perferendis\"></div></div><div class=\"voluptas\"></div></body></html>\n',5,NULL,NULL),(6,'en','In est sit sint expedita illo.','In est sit sint expedita illo.','Est adipisci qui fuga hic. Dolorum perspiciatis nam modi iste quia voluptate nostrum. Ea beatae at nemo voluptatem. Nam eum ducimus aspernatur possimus ut dolor.','Est adipisci qui fuga hic. Dolorum perspiciatis nam modi iste quia voluptate nostrum. Ea beatae at nemo voluptatem. Nam eum ducimus aspernatur possimus ut dolor.','<html><head><title>Nulla distinctio labore dolor.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">saepe</label><input type=\"text\" id=\"username\"><label for=\"password\">recusandae</label><input type=\"password\" id=\"password\"></form><div id=\"12681\"><b>Deserunt asperiores sit rerum eveniet modi accusantium fuga labore.</b><p>Ut et qui.</p></div><div id=\"73454\"></div><div class=\"perspiciatis\"><div id=\"59395\"></div><div id=\"66061\"></div><div id=\"97077\">Corrupti mollitia ipsum commodi.</div></div><div class=\"consequatur\"></div></body></html>\n',6,NULL,NULL),(7,'en','Corporis omnis deleniti rerum assumenda quas.','Corporis omnis deleniti rerum assumenda quas.','Ut qui earum aut dolorum corrupti repudiandae neque. Voluptatibus laudantium inventore dolores quia voluptatem soluta. Velit dolorem veritatis est tenetur et ex.','Ut qui earum aut dolorum corrupti repudiandae neque. Voluptatibus laudantium inventore dolores quia voluptatem soluta. Velit dolorem veritatis est tenetur et ex.','<html><head><title>Accusantium aut et deleniti sed enim odio eum ea.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">velit</label><input type=\"text\" id=\"username\"><label for=\"password\">ipsa</label><input type=\"password\" id=\"password\"></form><div id=\"94632\"></div><div class=\"reprehenderit\">Sed enim deleniti cupiditate dicta.<b>Modi assumenda culpa.</b></div><div class=\"officia\"><div class=\"consectetur\"><i>Sed laudantium distinctio non id.</i>Nihil harum delectus perferendis non at sed.Laboriosam et vero occaecati voluptatem dolores earum.<table><thead><tr><th>Et.</th><th>Vero ut.</th></tr></thead><tbody><tr><td>Ex perferendis cupiditate quo quia velit animi quia reiciendis tempora.</td><td>Quo voluptatem.</td></tr><tr><td>Placeat ipsam.</td><td>Qui aperiam.</td></tr><tr><td>Tempora illum adipisci et quis quis quibusdam.</td><td>Rerum iusto aut velit architecto pariatur delectus totam dolores sapiente voluptatum.</td></tr><tr><td>Nostrum pariatur culpa quis nemo et non sunt.</td><td>Aut vel culpa harum sit.</td></tr><tr><td>Est at rerum.</td><td>Et quaerat quam officia.</td></tr><tr><td>Impedit a voluptas omnis fuga.</td><td>Ipsam impedit facilis soluta.</td></tr><tr><td>Voluptas et asperiores sed voluptatem.</td><td>Ut enim est perspiciatis.</td></tr><tr><td>Iure ut.</td><td>Dicta laboriosam minus qui.</td></tr></tbody></table></div></div><div id=\"65746\"></div></body></html>\n',7,NULL,NULL),(8,'en','Animi qui ratione qui minima.','Animi qui ratione qui minima.','Excepturi expedita et quaerat. Aut recusandae provident laborum quod ducimus in commodi.','Excepturi expedita et quaerat. Aut recusandae provident laborum quod ducimus in commodi.','<html><head><title>Explicabo consectetur laboriosam.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">recusandae</label><input type=\"text\" id=\"username\"><label for=\"password\">corporis</label><input type=\"password\" id=\"password\"></form><div id=\"68107\"><a href=\"example.com\">Molestias quibusdam.</a>A ex ducimus quam rerum molestiae eum est iusto.<h1>Ut quibusdam in consequatur quidem magni nulla reiciendis omnis sit repellendus.</h1><span>Non id earum consequatur et molestiae accusantium.</span></div></body></html>\n',8,NULL,NULL),(9,'en','Repudiandae quis quia et.','Repudiandae quis quia et.','Amet quisquam laboriosam nulla minus. Blanditiis eius qui est suscipit. Non aliquid mollitia dolores.','Amet quisquam laboriosam nulla minus. Blanditiis eius qui est suscipit. Non aliquid mollitia dolores.','<html><head><title>Quisquam eligendi in id nihil vitae.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">soluta</label><input type=\"text\" id=\"username\"><label for=\"password\">deserunt</label><input type=\"password\" id=\"password\"></form><div class=\"commodi\"></div></body></html>\n',9,NULL,NULL),(10,'en','Earum repudiandae quia quo beatae adipisci accusamus.','Earum repudiandae quia quo beatae adipisci accusamus.','Ut et ipsum enim. Pariatur at sit ad. Ipsum praesentium labore impedit incidunt distinctio reprehenderit. Tenetur laudantium hic modi consequatur deserunt.','Ut et ipsum enim. Pariatur at sit ad. Ipsum praesentium labore impedit incidunt distinctio reprehenderit. Tenetur laudantium hic modi consequatur deserunt.','<html><head><title>Qui incidunt.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">pariatur</label><input type=\"text\" id=\"username\"><label for=\"password\">asperiores</label><input type=\"password\" id=\"password\"></form><div class=\"explicabo\"></div><div id=\"93038\"></div><div class=\"dolor\"><div class=\"quis\"></div><div id=\"24357\"></div><div id=\"53148\"><i>Praesentium voluptatem laudantium et consequatur rerum ut et.</i></div></div></body></html>\n',10,NULL,NULL),(11,'en','Expedita aut eum sed qui unde nobis ea.','Expedita aut eum sed qui unde nobis ea.','Qui eos saepe ut consequatur voluptas. Ullam iusto perferendis consectetur omnis qui doloribus consequatur. Dolorem ipsam ea molestiae iure dolorem qui. Quas fuga modi consectetur voluptatum asperiores.','Qui eos saepe ut consequatur voluptas. Ullam iusto perferendis consectetur omnis qui doloribus consequatur. Dolorem ipsam ea molestiae iure dolorem qui. Quas fuga modi consectetur voluptatum asperiores.','<html><head><title>Voluptatem eum facere temporibus praesentium qui quos mollitia porro.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">et</label><input type=\"text\" id=\"username\"><label for=\"password\">aut</label><input type=\"password\" id=\"password\"></form><div class=\"beatae\"></div><div class=\"exercitationem\"></div><div id=\"21704\"></div></body></html>\n',11,NULL,NULL),(12,'en','Numquam et ut placeat facilis.','Numquam et ut placeat facilis.','Excepturi temporibus ut eos. Omnis et voluptatum necessitatibus voluptatem beatae itaque quam tenetur.','Excepturi temporibus ut eos. Omnis et voluptatum necessitatibus voluptatem beatae itaque quam tenetur.','<html><head><title>Impedit rerum consequatur.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">molestiae</label><input type=\"text\" id=\"username\"><label for=\"password\">nihil</label><input type=\"password\" id=\"password\"></form><div id=\"22933\"></div></body></html>\n',12,NULL,NULL),(13,'en','Harum molestiae dolorum veniam rem et porro.','Harum molestiae dolorum veniam rem et porro.','Nostrum dignissimos enim qui quas. Ea qui ad molestiae eum sed voluptatem sit. Sapiente et enim quia consequuntur numquam ab fugiat. Nihil doloribus temporibus corrupti esse.','Nostrum dignissimos enim qui quas. Ea qui ad molestiae eum sed voluptatem sit. Sapiente et enim quia consequuntur numquam ab fugiat. Nihil doloribus temporibus corrupti esse.','<html><head><title>Non expedita dignissimos ducimus consequatur est totam.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">deleniti</label><input type=\"text\" id=\"username\"><label for=\"password\">et</label><input type=\"password\" id=\"password\"></form><div class=\"temporibus\"><p>Adipisci cum doloremque dolor non.</p></div><div id=\"67887\"><div class=\"minus\"></div><div class=\"et\"></div><div id=\"75784\"><p>Quam minima.</p><p>Doloremque fugiat.</p><ul><li>Cum a reiciendis repellat eveniet.</li><li>Saepe laborum numquam.</li><li>Ullam iusto.</li><li>Harum.</li><li>Esse pariatur sed.</li><li>Vel illo est.</li><li>Et laboriosam illum.</li><li>Sed reprehenderit dolorem ab sunt.</li><li>Consequatur et ducimus eos.</li><li>Consequatur nesciunt.</li><li>Voluptatum atque.</li></ul></div></div><div class=\"id\"></div><div class=\"et\"><div class=\"dicta\"></div><div class=\"labore\"></div></div></body></html>\n',13,NULL,NULL),(14,'en','Magnam id omnis sint quia eum excepturi provident dolore.','Magnam id omnis sint quia eum excepturi provident dolore.','Pariatur ratione aliquam reiciendis in qui dolores. Libero quis non earum laudantium vel. Et modi nobis illum et. Culpa aliquam et quam molestias.','Pariatur ratione aliquam reiciendis in qui dolores. Libero quis non earum laudantium vel. Et modi nobis illum et. Culpa aliquam et quam molestias.','<html><head><title>Cum voluptatem non voluptates.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">optio</label><input type=\"text\" id=\"username\"><label for=\"password\">facilis</label><input type=\"password\" id=\"password\"></form><div id=\"84142\"></div><div id=\"59036\"></div></body></html>\n',14,NULL,NULL),(15,'en','Praesentium illo nemo aut.','Praesentium illo nemo aut.','Aspernatur ratione dignissimos atque officiis explicabo iure adipisci. Sit quo est expedita facere non asperiores. Omnis et qui quis voluptatum nisi dolorum qui commodi.','Aspernatur ratione dignissimos atque officiis explicabo iure adipisci. Sit quo est expedita facere non asperiores. Omnis et qui quis voluptatum nisi dolorum qui commodi.','<html><head><title>Sed rerum excepturi quam sunt.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">rerum</label><input type=\"text\" id=\"username\"><label for=\"password\">blanditiis</label><input type=\"password\" id=\"password\"></form><div id=\"29699\"></div></body></html>\n',15,NULL,NULL),(16,'en','Atque aut qui aliquam vitae sed consequuntur cumque est.','Atque aut qui aliquam vitae sed consequuntur cumque est.','Officiis error harum consequatur assumenda magnam quia ut. Consequatur quo sed voluptas est. Mollitia consectetur quo et.','Officiis error harum consequatur assumenda magnam quia ut. Consequatur quo sed voluptas est. Mollitia consectetur quo et.','<html><head><title>Voluptatum fugit qui error at.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">nulla</label><input type=\"text\" id=\"username\"><label for=\"password\">doloremque</label><input type=\"password\" id=\"password\"></form><div id=\"70690\"><i>Quis voluptates.</i><h1>Ipsa eum officia.</h1><p>Atque est.</p></div><div class=\"id\"></div></body></html>\n',16,NULL,NULL),(17,'en','Assumenda saepe vero amet asperiores vel sint.','Assumenda saepe vero amet asperiores vel sint.','Qui natus atque sed minus provident est. Aut minus aliquid amet quos enim commodi. Ipsam doloremque similique non et reiciendis quis. Laboriosam a aut consequatur ex eligendi est aut quo. Eos ipsam ex nostrum neque sit omnis.','Qui natus atque sed minus provident est. Aut minus aliquid amet quos enim commodi. Ipsam doloremque similique non et reiciendis quis. Laboriosam a aut consequatur ex eligendi est aut quo. Eos ipsam ex nostrum neque sit omnis.','<html><head><title>Vero maiores voluptas dolor provident.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">rerum</label><input type=\"text\" id=\"username\"><label for=\"password\">eligendi</label><input type=\"password\" id=\"password\"></form><div id=\"14299\"></div></body></html>\n',17,NULL,NULL),(18,'en','Nihil sed asperiores est odit et quod hic sed.','Nihil sed asperiores est odit et quod hic sed.','Dicta vel occaecati asperiores quia illum doloribus non. Amet facere ipsam id id temporibus reiciendis. Molestiae aspernatur laboriosam qui nihil. Placeat esse minus quaerat inventore soluta natus voluptatem officia.','Dicta vel occaecati asperiores quia illum doloribus non. Amet facere ipsam id id temporibus reiciendis. Molestiae aspernatur laboriosam qui nihil. Placeat esse minus quaerat inventore soluta natus voluptatem officia.','<html><head><title>Autem error quidem explicabo consequatur velit harum et magni facilis iste sit dignissimos.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">earum</label><input type=\"text\" id=\"username\"><label for=\"password\">corporis</label><input type=\"password\" id=\"password\"></form><div id=\"37953\"><b>Sint.</b></div></body></html>\n',18,NULL,NULL),(19,'en','Eveniet vel quo et excepturi in omnis.','Eveniet vel quo et excepturi in omnis.','Nesciunt sunt qui sit porro placeat est. Ipsum possimus sunt hic est voluptatem.','Nesciunt sunt qui sit porro placeat est. Ipsum possimus sunt hic est voluptatem.','<html><head><title>Non voluptate.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">maxime</label><input type=\"text\" id=\"username\"><label for=\"password\">dolorem</label><input type=\"password\" id=\"password\"></form><div class=\"alias\"><div class=\"ipsa\"></div></div><div id=\"85089\"></div><div id=\"89046\"></div></body></html>\n',19,NULL,NULL),(20,'en','Non consequatur saepe error earum eaque ex.','Non consequatur saepe error earum eaque ex.','Soluta temporibus excepturi occaecati assumenda et ut minus. Id similique officiis vitae in aut et provident iure. Veniam id ut quia voluptas nobis ipsa adipisci.','Soluta temporibus excepturi occaecati assumenda et ut minus. Id similique officiis vitae in aut et provident iure. Veniam id ut quia voluptas nobis ipsa adipisci.','<html><head><title>Ipsa accusamus autem.</title></head><body><form action=\"example.com\" method=\"POST\"><label for=\"username\">eligendi</label><input type=\"text\" id=\"username\"><label for=\"password\">unde</label><input type=\"password\" id=\"password\"></form><div class=\"et\"></div><div class=\"dicta\"><h1>Nesciunt quas quos qui voluptatem.</h1><p>Voluptas dolores rerum.</p><a href=\"example.com\">Aut quos quaerat.</a></div></body></html>\n',20,NULL,NULL),(21,'en','Et aut molestiae provident cum.','Et aut molestiae provident cum.','Vel nulla et quod quo culpa nihil. Minus veritatis provident voluptatem officiis. Ea necessitatibus aliquam delectus sunt laudantium.','Vel nulla et quod quo culpa nihil. Minus veritatis provident voluptatem officiis. Ea necessitatibus aliquam delectus sunt laudantium.','<html><head><title>Est qui sit vitae veritatis qui et eveniet consequatur commodi.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">accusamus</label><input type=\"text\" id=\"username\"><label for=\"password\">et</label><input type=\"password\" id=\"password\"></form><div class=\"modi\"><b>Impedit adipisci.</b><a href=\"example.com\">Ex alias in.</a><table><thead><tr><th>Animi vel.</th><th>Et atque velit earum repudiandae in.</th><th>Architecto quo a.</th><th>Dicta sed ratione nisi.</th></tr></thead><tbody><tr><td>Natus officiis accusamus fuga natus.</td><td>Eos quam ipsam.</td><td>Consequuntur dolores.</td><td>Aliquam et impedit voluptatem vero necessitatibus fugiat cum.</td></tr></tbody></table><table><thead><tr><th>Et officiis.</th><th>Quia eligendi illo consequatur.</th><th>Nulla optio aliquam quisquam id.</th><th>Id repellat fugiat.</th><th>Tenetur suscipit corporis dignissimos eum.</th><th>Totam ut ipsam iusto.</th></tr></thead><tbody><tr><td>Pariatur rem ea architecto error excepturi placeat inventore.</td><td>A blanditiis quis corrupti nihil cum et quia.</td><td>Id sit molestiae ab voluptatum.</td><td>Numquam quibusdam numquam libero sed.</td><td>Cupiditate sed enim voluptatum consequatur aut.</td><td>Deserunt sed voluptatem quod eos aliquam non ipsa eaque totam asperiores nostrum omnis.</td></tr><tr><td>Reprehenderit et ab perspiciatis quaerat laborum.</td><td>Enim praesentium ad dolor.</td><td>Voluptatem corrupti quia minus.</td><td>Qui et quidem deserunt eligendi sequi quisquam aspernatur maxime.</td><td>Distinctio et voluptatem dignissimos dolores non rerum ad quibusdam rerum facere.</td><td>Eaque tenetur totam sed nobis.</td></tr><tr><td>Ut iste voluptatem voluptatem natus ipsum.</td><td>Similique voluptatem velit saepe qui neque.</td><td>Culpa aut ut architecto.</td><td>Iure doloribus molestiae veniam architecto tenetur tempore.</td><td>Non quo recusandae et iusto.</td><td>Quia quam consequatur aut ipsam.</td></tr><tr><td>Ipsa qui facilis illum sit veniam quisquam.</td><td>Quo quibusdam et.</td><td>Inventore unde iste.</td><td>Earum autem temporibus.</td><td>Vel in et qui quisquam.</td><td>Nihil commodi sint suscipit ab.</td></tr><tr><td>Iste.</td><td>Nesciunt.</td><td>Quo officiis harum molestias.</td><td>Deleniti perspiciatis modi ut qui magni.</td><td>Ipsum optio consequuntur quia facere et maiores ratione id saepe.</td><td>Id cupiditate.</td></tr><tr><td>Aut corrupti nemo minima eos dolores incidunt.</td><td>Fugit et rem eveniet ex ducimus eum provident atque iusto soluta officiis ex quos.</td><td>Quod commodi hic et.</td><td>Et architecto repellat.</td><td>Laboriosam.</td><td>Qui voluptatem porro velit.</td></tr><tr><td>Vero.</td><td>Impedit officiis rerum eveniet.</td><td>Impedit.</td><td>At rerum unde omnis voluptas omnis vel qui eos.</td><td>Labore nemo esse cumque laborum exercitationem labore rerum quo et.</td><td>Excepturi culpa quidem.</td></tr><tr><td>Eum vel reiciendis quia cupiditate at dolor eius ea accusamus.</td><td>Veritatis dicta provident eos quibusdam velit.</td><td>Nihil quae numquam est aut dolor.</td><td>Et asperiores.</td><td>Sit minus consectetur incidunt corporis.</td><td>Ratione similique nam ut ex quas porro tenetur eum.</td></tr></tbody></table></div><div id=\"84639\"></div></body></html>\n',21,NULL,NULL),(22,'en','Earum voluptatem veritatis itaque nihil.','Earum voluptatem veritatis itaque nihil.','Repellendus eveniet magni qui voluptatem. Quo reprehenderit perspiciatis assumenda consequatur sed aperiam. Quia et consequatur velit voluptates. Dolore distinctio voluptatem nobis non dolorem.','Repellendus eveniet magni qui voluptatem. Quo reprehenderit perspiciatis assumenda consequatur sed aperiam. Quia et consequatur velit voluptates. Dolore distinctio voluptatem nobis non dolorem.','<html><head><title>At nesciunt voluptatem dolore assumenda consequuntur nobis aut assumenda non.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">voluptatum</label><input type=\"text\" id=\"username\"><label for=\"password\">voluptatem</label><input type=\"password\" id=\"password\"></form><div class=\"molestiae\"><a href=\"example.com\">Eos a aut quis esse fuga qui rerum sequi enim.</a><i>Consectetur quod quod eveniet a.</i>A.Omnis omnis sit sequi dolore architecto nulla temporibus sit aut nemo.</div><div id=\"76440\"><div class=\"officiis\"></div><div class=\"labore\"></div><div class=\"doloribus\"></div><div class=\"unde\"></div></div><div id=\"73718\"></div><div id=\"56406\"><div id=\"20493\"></div><div class=\"adipisci\"></div><div class=\"modi\"><table><thead><tr><th>Modi et quos consectetur officia.</th><th>Asperiores aut tempora illum.</th><th>Dolores odio.</th></tr></thead><tbody><tr><td>Impedit illum maxime laborum quia distinctio voluptatum exercitationem sequi.</td><td>Voluptates pariatur aperiam.</td><td>Nobis minima aut quaerat voluptatem beatae.</td></tr><tr><td>Rem distinctio at velit qui quibusdam.</td><td>Asperiores asperiores ut laborum nemo molestiae est animi tempore.</td><td>Modi dolorem impedit odio dolorem voluptas sapiente beatae ea.</td></tr><tr><td>Ut quisquam corporis consequatur voluptate nostrum.</td><td>Omnis inventore vel provident excepturi voluptates.</td><td>Corrupti eligendi sit iusto itaque eaque cum facilis.</td></tr><tr><td>Dolor dicta ut.</td><td>Numquam.</td><td>Repudiandae ullam.</td></tr><tr><td>Est molestiae quas exercitationem eum minus dolorem quia tempore dignissimos soluta et.</td><td>Reprehenderit voluptatem nobis et in.</td><td>Totam fugit quia ad reprehenderit accusantium corporis debitis nulla.</td></tr></tbody></table><a href=\"example.net\">Impedit possimus nemo libero.</a></div><div class=\"et\"></div></div></body></html>\n',22,NULL,NULL),(23,'en','Quis ab quo quidem reiciendis ullam amet.','Quis ab quo quidem reiciendis ullam amet.','In a illo eius occaecati dolorem autem aspernatur est. Et officia officia consequatur iure veritatis architecto cumque et.','In a illo eius occaecati dolorem autem aspernatur est. Et officia officia consequatur iure veritatis architecto cumque et.','<html><head><title>Non voluptatem consequatur id accusantium.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">veritatis</label><input type=\"text\" id=\"username\"><label for=\"password\">nulla</label><input type=\"password\" id=\"password\"></form><div id=\"38735\"><div class=\"suscipit\"><a href=\"example.net\">Voluptas unde voluptatem.</a><span>Molestias perspiciatis adipisci blanditiis omnis eius.</span></div><div id=\"15203\"></div><div id=\"31609\"></div><div id=\"19397\"></div></div><div class=\"aspernatur\"></div><div id=\"85645\"><div class=\"optio\"></div><div id=\"33\"></div></div><div id=\"87576\"><a href=\"example.org\">Cum ad dolorem ipsam quia.</a><span>Ut est nihil esse.</span></div></body></html>\n',23,NULL,NULL),(24,'en','Soluta quia sint odio doloremque et.','Soluta quia sint odio doloremque et.','Temporibus ullam ut accusamus. Placeat modi hic dolore rerum quaerat ullam reiciendis qui. Qui provident cupiditate atque earum voluptate.','Temporibus ullam ut accusamus. Placeat modi hic dolore rerum quaerat ullam reiciendis qui. Qui provident cupiditate atque earum voluptate.','<html><head><title>Non placeat fugit autem.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">reiciendis</label><input type=\"text\" id=\"username\"><label for=\"password\">tempore</label><input type=\"password\" id=\"password\"></form><div class=\"tempore\"></div></body></html>\n',24,NULL,NULL),(25,'en','Vel quisquam eligendi consectetur et excepturi dignissimos.','Vel quisquam eligendi consectetur et excepturi dignissimos.','Qui omnis non numquam dolorem. Quaerat sint corporis quo assumenda libero labore. Recusandae cupiditate voluptas voluptas et dolorem dolore et nam. Consequuntur ipsam ipsa sapiente quasi voluptates ea.','Qui omnis non numquam dolorem. Quaerat sint corporis quo assumenda libero labore. Recusandae cupiditate voluptas voluptas et dolorem dolore et nam. Consequuntur ipsam ipsa sapiente quasi voluptates ea.','<html><head><title>Odio qui.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">reiciendis</label><input type=\"text\" id=\"username\"><label for=\"password\">enim</label><input type=\"password\" id=\"password\"></form><div class=\"amet\"><div class=\"dolorem\">Odit in et ipsum sed doloribus in et asperiores totam.</div><div class=\"beatae\"></div><div class=\"sit\"></div><div class=\"magni\"><span>Eaque eos.</span><a href=\"example.net\">Nobis rem in rem nihil nisi.</a><ul><li>Quia.</li><li>Architecto rerum eos.</li><li>Nemo recusandae aspernatur nam ut.</li><li>Et repudiandae.</li><li>Quo velit et quia.</li><li>Quidem voluptatem sunt id atque saepe.</li><li>Praesentium ipsa rerum aspernatur sit minus.</li><li>Ducimus natus.</li><li>Impedit excepturi.</li></ul></div></div></body></html>\n',25,NULL,NULL),(26,'en','Accusamus ea veritatis hic iusto illo.','Accusamus ea veritatis hic iusto illo.','Exercitationem et recusandae in et consequatur. Aut nobis quia et eum. Et atque molestias deserunt ut unde voluptatem.','Exercitationem et recusandae in et consequatur. Aut nobis quia et eum. Et atque molestias deserunt ut unde voluptatem.','<html><head><title>Molestiae ducimus reiciendis fugit neque nesciunt quam minima commodi.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">et</label><input type=\"text\" id=\"username\"><label for=\"password\">perferendis</label><input type=\"password\" id=\"password\"></form><div id=\"48877\"></div><div id=\"38518\"></div><div class=\"velit\"></div><div id=\"88554\"></div></body></html>\n',26,NULL,NULL),(27,'en','Doloremque a est dolor doloribus.','Doloremque a est dolor doloribus.','Pariatur reiciendis nihil et temporibus. Veritatis sit qui id impedit quia quo facilis temporibus. Ad id autem et.','Pariatur reiciendis nihil et temporibus. Veritatis sit qui id impedit quia quo facilis temporibus. Ad id autem et.','<html><head><title>Blanditiis deleniti officiis veritatis nostrum.</title></head><body><form action=\"example.org\" method=\"POST\"><label for=\"username\">ullam</label><input type=\"text\" id=\"username\"><label for=\"password\">doloremque</label><input type=\"password\" id=\"password\"></form><div id=\"11189\"></div><div id=\"91609\"><a href=\"example.net\">Nesciunt ipsum omnis asperiores enim ut consequuntur reiciendis consequatur facere tenetur.</a><a href=\"example.org\">Assumenda voluptates nostrum neque et.</a><a href=\"example.net\">A dicta consequatur soluta qui cupiditate quam voluptatem ipsum.</a><h1>Ex similique.</h1></div></body></html>\n',27,NULL,NULL),(28,'en','Qui illum vel eos cumque id.','Qui illum vel eos cumque id.','Error iste asperiores omnis et maiores. Molestias culpa cupiditate tenetur distinctio similique sed non. Autem vel assumenda et natus. Sunt delectus optio ut et eaque reiciendis nostrum.','Error iste asperiores omnis et maiores. Molestias culpa cupiditate tenetur distinctio similique sed non. Autem vel assumenda et natus. Sunt delectus optio ut et eaque reiciendis nostrum.','<html><head><title>Esse sint expedita consequatur quam dolores.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">est</label><input type=\"text\" id=\"username\"><label for=\"password\">maxime</label><input type=\"password\" id=\"password\"></form><div class=\"sit\"><div id=\"80028\"><table><thead><tr><th>Rerum rerum quod.</th><th>Ratione impedit animi.</th><th>Fuga deserunt architecto.</th></tr></thead><tbody><tr><td>Aliquam qui quia placeat aliquam et delectus repellat.</td><td>Ipsam ducimus dolores nihil recusandae quia provident repudiandae deleniti.</td><td>Ut illo debitis sint accusamus molestiae molestiae.</td></tr><tr><td>Nobis id rem ullam aspernatur optio sint eveniet nostrum repudiandae ad.</td><td>Et perspiciatis sunt voluptas aut assumenda voluptatem pariatur.</td><td>Facere debitis quos beatae.</td></tr><tr><td>Debitis sit placeat aut enim impedit harum.</td><td>Ut provident.</td><td>Ad sapiente perspiciatis voluptas quia corporis qui sed laborum.</td></tr><tr><td>Voluptatum quis magnam et expedita.</td><td>Ut et recusandae.</td><td>Reprehenderit sequi.</td></tr><tr><td>Quis ipsam.</td><td>Ducimus sapiente quas rem aut quis.</td><td>Sequi reprehenderit et.</td></tr></tbody></table><b>Sed.</b><h1>Est quae sapiente quia.</h1></div><div id=\"52320\"></div><div class=\"ea\"></div></div><div id=\"68159\"></div><div id=\"6672\"><div class=\"molestiae\"></div><div id=\"73365\"><p>Dolorum quisquam.</p></div><div id=\"33179\"></div><div id=\"42649\"></div></div><div id=\"78317\"><b>Deleniti ut natus pariatur ipsa est quo et natus.</b></div></body></html>\n',28,NULL,NULL),(29,'en','Explicabo sed omnis minima deleniti totam vel ex in.','Explicabo sed omnis minima deleniti totam vel ex in.','Dolore molestias exercitationem architecto et adipisci sunt sint saepe. Aliquam facilis voluptatem eius maxime. Ut mollitia et velit soluta ut voluptatem est. Quasi saepe quis blanditiis voluptas dolor et. Est magni quisquam labore.','Dolore molestias exercitationem architecto et adipisci sunt sint saepe. Aliquam facilis voluptatem eius maxime. Ut mollitia et velit soluta ut voluptatem est. Quasi saepe quis blanditiis voluptas dolor et. Est magni quisquam labore.','<html><head><title>Eveniet qui quo ad ratione a minus.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">tenetur</label><input type=\"text\" id=\"username\"><label for=\"password\">consectetur</label><input type=\"password\" id=\"password\"></form><div id=\"33872\"></div><div class=\"reprehenderit\"></div><div class=\"odio\"><div class=\"ea\"></div><div class=\"tenetur\"><h1>A et similique eum facere architecto non similique asperiores.</h1><b>Dolorem culpa voluptas doloremque.</b><i>Molestiae cum labore.</i></div><div id=\"91283\"><a href=\"example.net\">Enim aspernatur molestiae veritatis doloribus quae eius mollitia eius voluptas.</a></div><div id=\"25676\"><i>Dolor est facilis ratione quam rerum doloremque non qui qui.</i></div></div></body></html>\n',29,NULL,NULL),(30,'en','Quo pariatur vel consectetur consectetur in.','Quo pariatur vel consectetur consectetur in.','Eum officia delectus at eligendi sequi. Minima repellendus voluptatem ratione. Dolorem aliquid et perspiciatis. Ut aliquid illo ut dolorem eveniet dolore. Non aperiam voluptas qui omnis quo id tempora ullam.','Eum officia delectus at eligendi sequi. Minima repellendus voluptatem ratione. Dolorem aliquid et perspiciatis. Ut aliquid illo ut dolorem eveniet dolore. Non aperiam voluptas qui omnis quo id tempora ullam.','<html><head><title>Quisquam vitae saepe commodi eos quis numquam deserunt.</title></head><body><form action=\"example.net\" method=\"POST\"><label for=\"username\">officiis</label><input type=\"text\" id=\"username\"><label for=\"password\">qui</label><input type=\"password\" id=\"password\"></form><div class=\"quibusdam\"><div id=\"22492\"><i>Itaque quis est dolor et reiciendis et et voluptates et.</i><h1>Esse dicta explicabo sequi illo dolor earum animi non consequatur id nostrum.</h1><b>Iste sunt dolores repellat laudantium tempore.</b></div></div></body></html>\n',30,NULL,NULL);
/*!40000 ALTER TABLE `work_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `works`
--

DROP TABLE IF EXISTS `works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `works` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `works`
--

LOCK TABLES `works` WRITE;
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` VALUES (1,'publish','expedita-quisquam-quod-perspiciatis','2020-02-04 14:49:05','2020-02-04 14:49:05'),(2,'publish','id-itaque-optio-optio-consequatur-est-exercitationem','2020-02-04 14:49:05','2020-02-04 14:49:05'),(3,'publish','quia-in-ex-debitis-perferendis','2020-02-04 14:49:05','2020-02-04 14:49:05'),(4,'publish','magnam-autem-ad-omnis-culpa','2020-02-04 14:49:05','2020-02-04 14:49:05'),(5,'publish','amet-eos-fuga-reprehenderit-excepturi-totam-cumque-ut','2020-02-04 14:49:05','2020-02-04 14:49:05'),(6,'publish','in-est-sit-sint-expedita-illo','2020-02-04 14:49:05','2020-02-04 14:49:05'),(7,'publish','corporis-omnis-deleniti-rerum-assumenda-quas','2020-02-04 14:49:05','2020-02-04 14:49:05'),(8,'publish','animi-qui-ratione-qui-minima','2020-02-04 14:49:05','2020-02-04 14:49:05'),(9,'publish','repudiandae-quis-quia-et','2020-02-04 14:49:05','2020-02-04 14:49:05'),(10,'publish','earum-repudiandae-quia-quo-beatae-adipisci-accusamus','2020-02-04 14:49:05','2020-02-04 14:49:05'),(11,'publish','expedita-aut-eum-sed-qui-unde-nobis-ea','2020-02-04 14:49:05','2020-02-04 14:49:05'),(12,'publish','numquam-et-ut-placeat-facilis','2020-02-04 14:49:05','2020-02-04 14:49:05'),(13,'publish','harum-molestiae-dolorum-veniam-rem-et-porro','2020-02-04 14:49:05','2020-02-04 14:49:05'),(14,'publish','magnam-id-omnis-sint-quia-eum-excepturi-provident-dolore','2020-02-04 14:49:05','2020-02-04 14:49:05'),(15,'publish','praesentium-illo-nemo-aut','2020-02-04 14:49:05','2020-02-04 14:49:05'),(16,'publish','atque-aut-qui-aliquam-vitae-sed-consequuntur-cumque-est','2020-02-04 14:49:05','2020-02-04 14:49:05'),(17,'publish','assumenda-saepe-vero-amet-asperiores-vel-sint','2020-02-04 14:49:05','2020-02-04 14:49:05'),(18,'publish','nihil-sed-asperiores-est-odit-et-quod-hic-sed','2020-02-04 14:49:05','2020-02-04 14:49:05'),(19,'publish','eveniet-vel-quo-et-excepturi-in-omnis','2020-02-04 14:49:05','2020-02-04 14:49:05'),(20,'publish','non-consequatur-saepe-error-earum-eaque-ex','2020-02-04 14:49:05','2020-02-04 14:49:05'),(21,'publish','et-aut-molestiae-provident-cum','2020-02-04 14:49:05','2020-02-04 14:49:05'),(22,'publish','earum-voluptatem-veritatis-itaque-nihil','2020-02-04 14:49:05','2020-02-04 14:49:05'),(23,'publish','quis-ab-quo-quidem-reiciendis-ullam-amet','2020-02-04 14:49:05','2020-02-04 14:49:05'),(24,'publish','soluta-quia-sint-odio-doloremque-et','2020-02-04 14:49:05','2020-02-04 14:49:05'),(25,'publish','vel-quisquam-eligendi-consectetur-et-excepturi-dignissimos','2020-02-04 14:49:05','2020-02-04 14:49:05'),(26,'publish','accusamus-ea-veritatis-hic-iusto-illo','2020-02-04 14:49:05','2020-02-04 14:49:05'),(27,'publish','doloremque-a-est-dolor-doloribus','2020-02-04 14:49:05','2020-02-04 14:49:05'),(28,'publish','qui-illum-vel-eos-cumque-id','2020-02-04 14:49:05','2020-02-04 14:49:05'),(29,'publish','explicabo-sed-omnis-minima-deleniti-totam-vel-ex-in','2020-02-04 14:49:05','2020-02-04 14:49:05'),(30,'publish','quo-pariatur-vel-consectetur-consectetur-in','2020-02-04 14:49:05','2020-02-04 14:49:05');
/*!40000 ALTER TABLE `works` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'anthracite'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-07 11:18:29
